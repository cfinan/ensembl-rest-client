#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Notes
-----
To use the 'upload' functionality of this file, you must:
 $ pipenv install twine --dev
"""
from shutil import rmtree
from setuptools import find_packages, setup, Command
import io
import os
import sys

# Package meta-data.
NAME = "ensembl-rest-client"
"""The name of the package (`str`)
"""
DESCRIPTION = (
    'Python access to the Ensembl REST API'
)
"""Brief description of the package (`str`)
"""
URL = 'https://gitlab.com/cfinan/ensembl-rest-client'
"""Link to the project URL
"""
EMAIL = 'c.finan@ucl.ac.uk'
"""E-mail address of the package author (`str`)
"""
AUTHOR = 'Chris Finan'
"""Package author (`str`)
"""
REQUIRES_PYTHON = '>=3.7.0'
"""The required python version (`str`)
"""
VERSION = '0.1.3a0'
"""The version number (`str`)
"""
MANUAL_PACKAGES = [
]
"""Any additional packages that might not be found with find_packages
These will not be added if found (`list` of `str`).
"""
REQUIRED = [
    # 'requests', 'maya', 'records',
]
"""What packages are required for this module to be executed?
"""
EXTRAS = {
    # 'fancy feature': ['django'],
}
"""What packages are optional?
"""


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class UploadCommand(Command):
    """Support setup.py upload.
    """
    description = "Build and publish the package."
    user_options = []

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @staticmethod
    def status(s):
        """Prints things in bold.
        """
        print(r"\033[1m{0}\033[0m".format(s))

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def initialize_options(self):
        pass

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def finalize_options(self):
        pass

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def run(self):
        try:
            self.status('Removing previous builds...')
            rmtree(os.path.join(here, 'dist'))
        except OSError:
            pass

        self.status('Building Source and Wheel (universal) distribution…')
        os.system(
            '{0} setup.py sdist bdist_wheel --universal'.format(sys.executable)
        )

        self.status('Uploading the package to PyPI via Twine…')
        os.system('twine upload dist/*')

        self.status('Pushing git tags…')
        os.system('git tag v{0}'.format(about['__version__']))
        os.system('git push --tags')

        sys.exit()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_packages():
    """Get all the packages that need to be setup.

    Returns
    -------
    pkgs : `list` of `str`
        The packages to setup
    """
    # Manually add example datasets to the found packages
    pkgs = find_packages(
        exclude=["tests", "*.tests", "*.tests.*", "tests.*"]
    )

    for i in MANUAL_PACKAGES:
        if i not in pkgs:
            pkgs.append(i)
    return pkgs


# The rest you shouldn't have to touch too much :)
# ------------------------------------------------
# Except, perhaps the License and Trove Classifiers!
# If you do change the License, remember to change the Trove Classifier
# for that!
here = os.path.abspath(os.path.dirname(__file__))

try:
    # Import the README and use it as the long-description.
    # Note: this will only work if 'README.md' is present in your MANIFEST.in
    # file!
    with io.open(os.path.join(here, 'README.md'), encoding='utf-8') as f:
        long_description = '\n' + f.read()
except FileNotFoundError:
    # Fall back to the description
    long_description = DESCRIPTION

# Load the package's __version__.py module as a dictionary.
about = {}
if not VERSION:
    project_slug = NAME.lower().replace("-", "_").replace(" ", "_")
    with open(os.path.join(here, project_slug, '_version.py')) as f:
        exec(f.read(), about)
else:
    about['__version__'] = VERSION

# Get the packages to setup
pkgs = get_packages()

# Where the magic happens:
setup(
    name=NAME,
    version=about['__version__'],
    description=DESCRIPTION,
    long_description=long_description,
    long_description_content_type='text/markdown',
    author=AUTHOR,
    author_email=EMAIL,
    python_requires=REQUIRES_PYTHON,
    url=URL,
    # Packages defined above
    packages=pkgs,
    # entry_points={
    #     "console_scripts": [
    #     ],
    # },
    install_requires=REQUIRED,
    extras_require=EXTRAS,
    include_package_data=True,
    license='GPLv3+',
    classifiers=[
        # Trove classifiers
        # Full list: https://pypi.python.org/pypi?%3Aaction=list_classifiers
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: Implementation :: PyPy'
    ],
    # $ setup.py publish support.
    # cmdclass={
    #     'upload': UploadCommand,
    # },
)
