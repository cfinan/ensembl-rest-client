"""Perform queries against the transcript haplotypes endpoints
"""
from ensembl_rest_client.base import base_rest_client as brc


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class _TranscriptHaplotypes(object):
    """Perform TranscriptHaplotypes queries
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_transcript_haplotypes(self, query_id, species,
                                  aligned_sequences=None, samples=None,
                                  sequence=None, norm_func=brc.default_pandas):
        """
        Computes observed transcript haplotype sequences based on phased
        genotype data. GET method. Queries:
        `transcript_haplotypes/:species/:id <https://rest.ensembl.org/documentation/info/transcript_haplotypes_get>`_.

        Parameters
        ----------
        query_id : str
            Transcript stable id. Example values: 'ENST00000288602'.
        species : str
            Species name/alias. Example values: 'homo_sapiens'.
        aligned_sequences : bool or NoneType, optional, default: NoneType
            Include aligned sequences used to generate differences. Example
            values: '1'.
        samples : bool or NoneType, optional, default: NoneType
            Include sample-haplotype assignments. Example values: '1'.
        sequence : bool or NoneType, optional, default: NoneType
            Include raw sequences. Example values: '1'.
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use when obj.pandas is True

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'GET'
        endpoint = 'transcript_haplotypes/{species}/{id}'.format(
            species=species,
            id=query_id
        )

        return self.rest_query(
            method_type,
            endpoint,
            aligned_sequences=aligned_sequences,
            samples=samples,
            sequence=sequence,
            norm_func=norm_func
        )


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class TranscriptHaplotypes(brc._BaseRestClient, _TranscriptHaplotypes):
    """Perform TranscriptHaplotypes queries
    """
    pass
