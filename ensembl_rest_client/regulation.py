"""Perform queries against the regulation endpoints
"""
from ensembl_rest_client.base import base_rest_client as brc


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class _Regulation(object):
    """Perform Regulation queries
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_regulatory_species_microarray_vendor(self, microarray, species,
                                                 vendor,
                                                 norm_func=brc.default_pandas):
        """
        Returns information about a specific microarray. GET method. Queries:
        `regulatory/species/:species/microarray/:microarray/vendor/:vendor <https://rest.ensembl.org/documentation/info/array>`_.

        Parameters
        ----------
        microarray : str
            Microarray name. Example values: 'HumanWG_6_V2'.
        species : str
            Species name/alias. Example values: 'homo_sapiens'.
        vendor : str
            Probe name. Example values: 'ILMN_1763508'.
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use when obj.pandas is True

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'GET'
        endpoint = (
            'regulatory/species/{species}/microarray/{microarray}/vendor'
            '/{vendor}'.format(
                species=species,
                microarray=microarray,
                vendor=vendor
            )
        )

        return self.rest_query(
            method_type,
            endpoint,
            norm_func=norm_func
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_regulatory_species_epigenome(self, species,
                                         norm_func=brc.default_pandas):
        """
        Returns information about all epigenomes available for the given
        species. GET method. Queries:
        `regulatory/species/:species/epigenome <https://rest.ensembl.org/documentation/info/fetch_all_epigenomes>`_.

        Parameters
        ----------
        species : str
            Species name/alias. Example values: 'homo_sapiens'.
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use when obj.pandas is True

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'GET'
        endpoint = 'regulatory/species/{species}/epigenome'.format(
            species=species
        )

        return self.rest_query(
            method_type,
            endpoint,
            norm_func=norm_func
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_species_binding_matrix(self, binding_matrix, species, unit=None,
                                   norm_func=brc.default_pandas):
        """
        Return the specified binding matrix. GET method. Queries:
        `species/:species/binding_matrix/:binding_matrix_stable_id/ <https://rest.ensembl.org/documentation/info/get_binding_matrix>`_.

        Parameters
        ----------
        binding_matrix : str
            Stable ID of binding matrix. Example values: 'ENSPFM0001'.
        species : str
            Species name/alias. Example values: 'homo_sapiens'.
        unit : str or NoneType, optional, default: NoneType
            Unit of the matrix elements. Example values: 'frequencies,
            probabilities, bits'.
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use when obj.pandas is True

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'GET'
        endpoint = (
            'species/{species}/binding_matrix/{binding_matrix}'.format(
                species=species,
                binding_matrix=binding_matrix
            )
        )

        return self.rest_query(
            method_type,
            endpoint,
            unit=unit,
            norm_func=norm_func
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_regulatory_species_microarray(self, species,
                                          norm_func=brc.default_pandas):
        """
        Returns information about all microarrays available for the given
        species. GET method. Queries:
        `regulatory/species/:species/microarray <https://rest.ensembl.org/documentation/info/list_all_microarrays>`_.

        Parameters
        ----------
        species : str
            Species name/alias. Example values: 'homo_sapiens'.
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use when obj.pandas is True

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'GET'
        endpoint = 'regulatory/species/{species}/microarray'.format(
            species=species
        )

        return self.rest_query(
            method_type,
            endpoint,
            norm_func=norm_func
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_regulatory_species_microarray_probe(self, microarray, probe,
                                                species, gene=None,
                                                transcripts=None,
                                                norm_func=brc.default_pandas):
        """
        Returns information about a specific probe from a microarray. GET
        method. Queries:
        `regulatory/species/:species/microarray/:microarray/probe/:probe <https://rest.ensembl.org/documentation/info/probe>`_.

        Parameters
        ----------
        microarray : str
            Microarray name. Example values: 'HumanWG_6_V2'.
        probe : str
            Probe name. Example values: 'ILMN_1763508'.
        species : str
            Species name/alias. Example values: 'homo_sapiens'.
        gene : bool or NoneType, optional, default: NoneType
            Has to be used in conjunction with transcript. Displays the
            associated gene.
        transcripts : bool or NoneType, optional, default: NoneType
            Displays the transcripts linked to this probe.
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use when obj.pandas is True

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'GET'
        endpoint = (
            'regulatory/species/{species}/microarray/{microarray}/'
            'probe/{probe}'.format(
                species=species,
                microarray=microarray,
                probe=probe
            )
        )

        return self.rest_query(
            method_type,
            endpoint,
            gene=gene,
            transcripts=transcripts,
            norm_func=norm_func
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_regulatory_species_microarray_probe_set(self, microarray,
                                                    probe_set, species,
                                                    gene=None,
                                                    transcripts=None,
                                                    norm_func=brc.default_pandas):
        """
        Returns information about a specific probe_set from a microarray. GET
        method. Queries:
        `regulatory/species/:species/microarray/:microarray/probe_set/:probe_set <https://rest.ensembl.org/documentation/info/probe_set>`_.

        Parameters
        ----------
        microarray : str
            Microarray name. Example values: 'HG-U133_Plus_2'.
        probe_set : str
            ProbeSet name. Example values: '202820_at'.
        species : str
            Species name/alias. Example values: 'homo_sapiens'.
        gene : bool or NoneType, optional, default: NoneType
            Has to be used in conjunction with transcript. Displays the
            associated gene.
        transcripts : bool or NoneType, optional, default: NoneType
            Displays the transcripts linked to this probe.
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use when obj.pandas is True

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'GET'
        endpoint = (
            'regulatory/species/{species}/microarray/{microarray}'
            '/probe_set/{probe_set}'.format(
                species=species,
                microarray=microarray,
                probe_set=probe_set
            )
        )

        return self.rest_query(
            method_type,
            endpoint,
            gene=gene,
            transcripts=transcripts,
            norm_func=norm_func
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_regulatory_species_id(self, query_id, species, activity=None,
                                  norm_func=brc.default_pandas):
        """
        Returns a RegulatoryFeature given its stable ID (e.g. ENSR00000082023).
        GET method. Queries:
        `regulatory/species/:species/id/:id <https://rest.ensembl.org/documentation/info/regulatory_id>`_.

        Parameters
        ----------
        query_id : str
            RegulatoryFeature stable ID. Example values: 'ENSR00000082023'.
        species : str
            Species name/alias. Example values: 'homo_sapiens'.
        activity : bool or NoneType, optional, default: NoneType
            Returns the activity of the Regulatory Feature in each Epigenome
            for the given species.
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use when obj.pandas is True

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'GET'
        endpoint = 'regulatory/species/{species}/id/{id}'.format(
            species=species,
            id=query_id
        )

        return self.rest_query(
            method_type,
            endpoint,
            activity=activity,
            norm_func=norm_func
        )


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Regulation(brc._BaseRestClient, _Regulation):
    """Perform Regulation queries
    """
    pass
