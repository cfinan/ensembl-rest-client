"""Perform queries against the mapping endpoints
"""
from ensembl_rest_client.base import base_rest_client as brc


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class _Mapping(object):
    """Perform Mapping queries
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_map_cdna(self, query_id, region, include_original_region=None,
                     species=None, norm_func=brc.default_pandas):
        """
        Convert from cDNA coordinates to genomic coordinates. Output reflects
        forward orientation coordinates as returned from the Ensembl API. GET
        method. Queries:
        `map/cdna/:id/:region <https://rest.ensembl.org/documentation/info/assembly_cdna>`_.

        Parameters
        ----------
        query_id : str
            An Ensembl stable ID. Example values: 'ENST00000288602'.
        region : str
            Query region. Example values: '100..300'.
        include_original_region : bool or NoneType, optional, default: NoneType
            Include original input region (cDNA coordinates) along with the
            target region (genomic coordinates) mappings.
        species : str or NoneType, optional, default: NoneType
            Species name/alias. Example values: 'homo_sapiens human'.
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use when obj.pandas is True

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'GET'
        endpoint = 'map/cdna/{id}/{region}'.format(
            id=query_id,
            region=region
        )

        return self.rest_query(
            method_type,
            endpoint,
            include_original_region=include_original_region,
            species=species,
            norm_func=norm_func
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_map_cds(self, query_id, region, include_original_region=None,
                    species=None, norm_func=brc.default_pandas):
        """
        Convert from CDS coordinates to genomic coordinates. Output reflects
        forward orientation coordinates as returned from the Ensembl API. GET
        method. Queries:
        `map/cds/:id/:region <https://rest.ensembl.org/documentation/info/assembly_cds>`_.

        Parameters
        ----------
        query_id : str
            An Ensembl stable ID. Example values: 'ENST00000288602'.
        region : str
            Query region. Example values: '1..1000'.
        include_original_region : bool or NoneType, optional, default: NoneType
            Include original input region (cds coordinates) along with the
            target region (genomic coordinates) mappings.
        species : str or NoneType, optional, default: NoneType
            Species name/alias. Example values: 'homo_sapiens human'.
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use when obj.pandas is True

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'GET'
        endpoint = 'map/cds/{id}/{region}'.format(
            id=query_id,
            region=region
        )

        return self.rest_query(
            method_type,
            endpoint,
            include_original_region=include_original_region,
            species=species,
            norm_func=norm_func
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_map(self, asm_one, asm_two, region, species, coord_system=None,
                target_coord_system=None, norm_func=brc.default_pandas):
        """
        Convert the co-ordinates of one assembly to another. GET method.
        Queries:
        `map/:species/:asm_one/:region/:asm_two <https://rest.ensembl.org/documentation/info/assembly_map>`_.

        Parameters
        ----------
        asm_one : str
            Version of the input assembly. Example values: 'GRCh37'.
        asm_two : str
            Version of the output assembly. Example values: 'GRCh38'.
        region : str
            Query region. Example values: 'X:1000000..1000100:1
            X:1000000..1000100:-1 X:1000000..1000100'.
        species : str
            Species name/alias. Example values: 'homo_sapiens human'.
        coord_system : str or NoneType, optional, default: NoneType
            Name of the input coordinate system. Example values: 'chromosome'.
        target_coord_system : str or NoneType, optional, default: NoneType
            Name of the output coordinate system. Example values: 'chromosome'.
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use when obj.pandas is True

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'GET'
        endpoint = 'map/{species}/{asm_one}/{region}/{asm_two}'.format(
            species=species,
            asm_one=asm_one,
            region=region,
            asm_two=asm_two
        )

        return self.rest_query(
            method_type,
            endpoint,
            coord_system=coord_system,
            target_coord_system=target_coord_system,
            norm_func=norm_func
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_map_translation(self, query_id, region, species=None,
                            norm_func=brc.default_pandas):
        """
        Convert from protein (translation) coordinates to genomic coordinates.
        Output reflects forward orientation coordinates as returned from the
        Ensembl API. GET method. Queries:
        `map/translation/:id/:region <https://rest.ensembl.org/documentation/info/assembly_translation>`_.

        Parameters
        ----------
        query_id : str
            An Ensembl stable ID. Example values: 'ENSP00000288602'.
        region : str
            Query region. Example values: '100..300'.
        species : str or NoneType, optional, default: NoneType
            Species name/alias. Example values: 'homo_sapiens human'.
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use when obj.pandas is True

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'GET'
        endpoint = 'map/translation/{id}/{region}'.format(
            id=query_id,
            region=region
        )

        return self.rest_query(
            method_type,
            endpoint,
            species=species,
            norm_func=norm_func
        )


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Mapping(brc._BaseRestClient, _Mapping):
    """Perform Mapping queries
    """
    pass
