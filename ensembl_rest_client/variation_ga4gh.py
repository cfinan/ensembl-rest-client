"""Perform queries against the variation GA4GH endpoints
"""
from ensembl_rest_client.base import base_rest_client as brc


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class _VariationGa4Gh(object):
    """Perform VariationGa4Gh queries
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_ga4gh_beacon(self, norm_func=brc.default_pandas):
        """
        Return Beacon information. GET method. Queries:
        `ga4gh/beacon <https://rest.ensembl.org/documentation/info/beacon_get>`_.

        Parameters
        ----------
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use when obj.pandas is True

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'GET'
        endpoint = 'ga4gh/beacon'

        return self.rest_query(
            method_type,
            endpoint,
            norm_func=norm_func
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_ga4gh_beacon_query(self, alternate_bases, assembly_id, end,
                               end_max, end_min, reference_bases,
                               reference_name, start, start_max, start_min,
                               variant_type, dataset_ids=None,
                               include_dataset_responses=None,
                               norm_func=brc.default_pandas):
        """
        Return the Beacon response for allele information. GET method. Queries:
        `ga4gh/beacon/query <https://rest.ensembl.org/documentation/info/beacon_query_get>`_.

        Parameters
        ----------
        alternate_bases : str
            The bases that appear instead of the reference bases. Accepted
            values: see the ALT field in VCF 4.2 specification
            (http://samtools.github.io/hts-specs/VCFv4.2.pdf) Note: either
            alternateBases or variantType is required. Example values: 'C'.
        assembly_id : str
            Assembly identifier (GRC notation, e.g. GRCh38). Example values:
            'GRCh38'.
        end : int
            Precise end position, allele locus (0-based). Accepted values: non-
            negative integers smaller than reference length. Note: only
            required when using variantType to query precise structural
            variants. Example values: '__VAR(GA4GH_beacon_end)__'.
        end_max : int
            Maximum end coordinate, allele locus (0-based). Accepted values:
            non-negative integers smaller than reference length. Note: only
            required when using variantType to query imprecise structural
            variants. Example values: '__VAR(GA4GH_beacon_endmax)__'.
        end_min : int
            Minimum end coordinate, allele locus (0-based). Accepted values:
            non-negative integers smaller than reference length. Note: only
            required when using variantType to query imprecise structural
            variants. Example values: '__VAR(GA4GH_beacon_endmin)__'.
        reference_bases : str
            Reference bases for this variant (starting from start). Accepted
            values: see the REF field in VCF 4.2 specification
            (http://samtools.github.io/hts-specs/VCFv4.2.pdf). Example values:
            'G'.
        reference_name : str
            Reference name (chromosome). Accepted values: 1-22, X, Y, MT.
            Example values: '9'.
        start : int
            Precise start position, allele locus (0-based). Accepted values:
            non-negative integers smaller than reference length. Note: Do not
            use when querying for imprecise structural variants, use startMin
            and startMax instead. Example values: '22125503'.
        start_max : int
            Maximum start coordinate, allele locus (0-based). Accepted values:
            non-negative integers smaller than reference length. Note: only
            required when using variantType to query imprecise structural
            variants. Example values: '__VAR(GA4GH_beacon_startmax)__'.
        start_min : int
            Minimum start coordinate, allele locus (0-based). Accepted values:
            non-negative integers smaller than reference length. Note: only
            required when using variantType to query imprecise structural
            variants. Example values: '__VAR(GA4GH_beacon_startmin)__'.
        variant_type : str
            Used to denote structural variants. Accepted values:
            DUP,DEL,INS,INV,CNV,DUP:TANDEM, see the ALT field in VCF 4.2
            specification (http://samtools.github.io/hts-specs/VCFv4.2.pdf)
            Note: either alternateBases or variantType is required. Example
            values: '__VAR(GA4GH_beacon_variantType)__'.
        dataset_ids : array of strings or NoneType, optional, default: NoneType
            Identifiers of datasets. Identifiers have to be chosen from 'Short
            name' column in the Variant sets list
            (http://www.ensembl.org/info/genome/variation/species/sets.html).
        include_dataset_responses : str or NoneType, optional, default: NoneType
            Indicator of whether responses for individual datasets should be
            included. Accepted values: ALL, HIT, MISS, NONE.
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use when obj.pandas is True

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'GET'
        endpoint = 'ga4gh/beacon/query'

        return self.rest_query(
            method_type,
            endpoint,
            alternateBases=alternate_bases,
            assemblyId=assembly_id,
            end=end,
            endMax=end_max,
            endMin=end_min,
            referenceBases=reference_bases,
            referenceName=reference_name,
            start=start,
            startMax=start_max,
            startMin=start_min,
            variantType=variant_type,
            datasetIds=dataset_ids,
            includeDatasetResponses=include_dataset_responses,
            norm_func=norm_func
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def post_ga4gh_beacon_query(self, alternate_bases, assembly_id, end,
                                end_max, end_min, reference_bases,
                                reference_name, start, start_max, start_min,
                                variant_type, dataset_ids=None,
                                include_dataset_responses=None,
                                norm_func=brc.default_pandas):
        """
        Return the Beacon response for allele information. POST method. Queries:
        `ga4gh/beacon/query <https://rest.ensembl.org/documentation/info/beacon_query_post>`_.

        Parameters
        ----------
        alternate_bases : str
            The bases that appear instead of the reference bases. Accepted
            values: see the ALT field in VCF 4.2 specification
            (http://samtools.github.io/hts-specs/VCFv4.2.pdf) Note: either
            alternateBases or variantType is required. Example values: 'C'.
        assembly_id : str
            Assembly identifier (GRC notation, e.g. GRCh38). Example values:
            'GRCh38'.
        end : int
            Precise end position, allele locus (0-based). Accepted values: non-
            negative integers smaller than reference length. Note: only
            required when using variantType to query precise structural
            variants. Example values: '__VAR(GA4GH_beacon_end)__'.
        end_max : int
            Maximum end coordinate, allele locus (0-based). Accepted values:
            non-negative integers smaller than reference length. Note: only
            required when using variantType to query imprecise structural
            variants. Example values: '__VAR(GA4GH_beacon_endmax)__'.
        end_min : int
            Minimum end coordinate, allele locus (0-based). Accepted values:
            non-negative integers smaller than reference length. Note: only
            required when using variantType to query imprecise structural
            variants. Example values: '__VAR(GA4GH_beacon_endmin)__'.
        reference_bases : str
            Reference bases for this variant (starting from start). Accepted
            values: see the REF field in VCF 4.2 specification
            (http://samtools.github.io/hts-specs/VCFv4.2.pdf). Example values:
            'G'.
        reference_name : str
            Reference name (chromosome). Accepted values: 1-22, X, Y, MT.
            Example values: '9'.
        start : int
            Precise start position, allele locus (0-based). Accepted values:
            non-negative integers smaller than reference length. Note: Do not
            use when querying for imprecise structural variants, use startMin.
            Example values: '22125503'.
        start_max : int
            Maximum start coordinate, allele locus (0-based). Accepted values:
            non-negative integers smaller than reference length. Note: only
            required when using variantType to query imprecise structural
            variants. Example values: '__VAR(GA4GH_beacon_startmax)__'.
        start_min : int
            Minimum start coordinate, allele locus (0-based). Accepted values:
            non-negative integers smaller than reference length. Note: only
            required when using variantType to query imprecise structural
            variants. Example values: '__VAR(GA4GH_beacon_startmin)__'.
        variant_type : str
            Used to denote structural variants. Accepted values:
            DUP,DEL,INS,INV,CNV,DUP:TANDEM, see the ALT field in VCF 4.2
            specification (http://samtools.github.io/hts-specs/VCFv4.2.pdf)
            Note: either alternateBases or variantType is required. Example
            values: '__VAR(GA4GH_beacon_variantType)__'.
        dataset_ids : array of strings or NoneType, optional, default: NoneType
            Identifiers of datasets. Identifiers have to be chosen from 'Short
            name' column in the Variant sets list
            (http://www.ensembl.org/info/genome/variation/species/sets.html).
        include_dataset_responses : str or NoneType, optional, default: NoneType
            Indicator of whether responses for individual datasets should be
            included. Accepted values: ALL, HIT, MISS, NONE.
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use when obj.pandas is True

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'POST'
        endpoint = 'ga4gh/beacon/query'

        return self.rest_query(
            method_type,
            endpoint,
            alternateBases=alternate_bases,
            assemblyId=assembly_id,
            end=end,
            endMax=end_max,
            endMin=end_min,
            referenceBases=reference_bases,
            referenceName=reference_name,
            start=start,
            startMax=start_max,
            startMin=start_min,
            variantType=variant_type,
            datasetIds=dataset_ids,
            includeDatasetResponses=include_dataset_responses,
            norm_func=norm_func
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_ga4gh_features(self, query_id, norm_func=brc.default_pandas):
        """
        Return the GA4GH record for a specific sequence feature given its
        identifier. GET method. Queries:
        `ga4gh/features/:id <https://rest.ensembl.org/documentation/info/features_id>`_.

        Parameters
        ----------
        query_id : str
            Feature id. Example values: 'ENST00000408937.7'.
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use when obj.pandas is True

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'GET'
        endpoint = 'ga4gh/features/{id}'.format(
            id=query_id
        )

        return self.rest_query(
            method_type,
            endpoint,
            norm_func=norm_func
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def post_ga4gh_features_search(self, end, reference_name, start,
                                   feature_set_id, feature_types=None,
                                   featureset_id=None, page_size=None,
                                   page_token=None, parent_id=None,
                                   norm_func=brc.default_pandas):
        """
        Return a list of sequence annotation features in GA4GH format. POST
        method. Queries:
        `ga4gh/features/search <https://rest.ensembl.org/documentation/info/features_post>`_.

        Parameters
        ----------
        end : int
            Return features within a window with this end location. Example
            values: '1109000'.
        reference_name : str
            Return features on this reference. Example values: '6'.
        start : int
            Return features within a window with this start location. Example
            values: '1108000'.
        feature_set_id : str
            Post option not defined in required/optional args.
        feature_types : array of strings or NoneType, optional, default: NoneType
            Return features of this type (requires SO terms). Example values:
            '[transcript]'.
        featureset_id : str or NoneType, optional, default: NoneType
            Return features in this set. Example values: 'Ensembl'.
        page_size : int or NoneType, optional, default: NoneType
            Number of features to return per request.
        page_token : int or NoneType, optional, default: NoneType
            Identifier showing which page of data to retrieve next.
        parent_id : str or NoneType, optional, default: NoneType
            Return the child features of this feature. Example values:
            'ENSG00000176515.1'.
        """
        method_type = 'POST'
        endpoint = 'ga4gh/features/search'

        return self.rest_query(
            method_type,
            endpoint,
            end=end,
            referenceName=reference_name,
            start=start,
            featureSetId=feature_set_id,
            featureTypes=feature_types,
            featuresetId=featureset_id,
            pageSize=page_size,
            pageToken=page_token,
            parentId=parent_id,
            norm_func=norm_func
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def post_ga4gh_callsets_search(self, variant_set_id, name=None,
                                   page_size=None, page_token=None,
                                   norm_func=brc.default_pandas):
        """
        Return a list of sets of genotype calls for specific samples in GA4GH
        format. POST method. Queries:
        `ga4gh/callsets/search <https://rest.ensembl.org/documentation/info/gacallSet>`_.

        Parameters
        ----------
        variant_set_id : str
            Return callSets for a specific variant set. Example values: '1'.
        name : str or NoneType, optional, default: NoneType
            Return callSets by name. Example values: 'NA19777'.
        page_size : int or NoneType, optional, default: NoneType
            Number of callSets to return per request.
        page_token : int or NoneType, optional, default: NoneType
            Identifier showing which page of data to retrieve next.
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use when obj.pandas is True

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'POST'
        endpoint = 'ga4gh/callsets/search'

        return self.rest_query(
            method_type,
            endpoint,
            variantSetId=variant_set_id,
            name=name,
            pageSize=page_size,
            pageToken=page_token,
            norm_func=norm_func
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_ga4gh_callsets(self, query_id, norm_func=brc.default_pandas):
        """
        Return the GA4GH record for a specific CallSet given its identifier.
        GET method. Queries:
        `ga4gh/callsets/:id <https://rest.ensembl.org/documentation/info/gacallset_id>`_.

        Parameters
        ----------
        query_id : str
            CallSet id. Example values: '1'.
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use when obj.pandas is True

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'GET'
        endpoint = 'ga4gh/callsets/{id}'.format(
            id=query_id
        )

        return self.rest_query(
            method_type,
            endpoint,
            norm_func=norm_func
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def post_ga4gh_datasets_search(self, page_size=None, page_token=None,
                                   norm_func=brc.default_pandas):
        """
        Return a list of datasets in GA4GH format. POST method. Queries:
        `ga4gh/datasets/search <https://rest.ensembl.org/documentation/info/gadataset>`_.

        Parameters
        ----------
        page_size : int or NoneType, optional, default: NoneType
            Number of dataSets to return per request.
        page_token : int or NoneType, optional, default: NoneType
            Identifier showing which page of data to retrieve next.
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use when obj.pandas is True

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'POST'
        endpoint = 'ga4gh/datasets/search'

        return self.rest_query(
            method_type,
            endpoint,
            pageSize=page_size,
            pageToken=page_token,
            norm_func=norm_func
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_ga4gh_datasets(self, query_id, norm_func=brc.default_pandas):
        """
        Return the GA4GH record for a specific dataset given its identifier.
        GET method. Queries:
        `ga4gh/datasets/:id <https://rest.ensembl.org/documentation/info/gadataset_id>`_.

        Parameters
        ----------
        query_id : str
            Dataset id. Example values: '6e340c4d1e333c7a676b1710d2e3953c'.
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use when obj.pandas is True

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'GET'
        endpoint = 'ga4gh/datasets/{id}'.format(
            id=query_id
        )

        return self.rest_query(
            method_type,
            endpoint,
            norm_func=norm_func
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def post_ga4gh_featuresets_search(self, dataset_id, page_size=None,
                                      page_token=None,
                                      norm_func=brc.default_pandas):
        """
        Return a list of feature sets in GA4GH format. POST method. Queries:
        `ga4gh/featuresets/search <https://rest.ensembl.org/documentation/info/gafeatureset>`_.

        Parameters
        ----------
        dataset_id : str
            Return featureSets by dataSet Identifier. Example values:
            'Ensembl'.
        page_size : int or NoneType, optional, default: NoneType
            Number of featureSets to return per request.
        page_token : int or NoneType, optional, default: NoneType
            Identifier showing which page of data to retrieve next.
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use when obj.pandas is True

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'POST'
        endpoint = 'ga4gh/featuresets/search'

        return self.rest_query(
            method_type,
            endpoint,
            datasetId=dataset_id,
            pageSize=page_size,
            pageToken=page_token,
            norm_func=norm_func
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_ga4gh_featuresets(self, query_id, norm_func=brc.default_pandas):
        """
        Return the GA4GH record for a specific featureSet given its identifier.
        GET method. Queries:
        `ga4gh/featuresets/:id <https://rest.ensembl.org/documentation/info/gafeatureset_id>`_.

        Parameters
        ----------
        query_id : str
            featureSet id. Example values: 'Ensembl'.
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use when obj.pandas is True

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'GET'
        endpoint = 'ga4gh/featuresets/{id}'.format(
            id=query_id
        )

        return self.rest_query(
            method_type,
            endpoint,
            norm_func=norm_func
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_ga4gh_variants(self, query_id, norm_func=brc.default_pandas):
        """
        Return the GA4GH record for a specific variant given its identifier.
        GET method. Queries:
        `ga4gh/variants/:id <https://rest.ensembl.org/documentation/info/gavariant_id>`_.

        Parameters
        ----------
        query_id : str
            Variation id. Example values: '1:rs1333049'.
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use when obj.pandas is True

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'GET'
        endpoint = 'ga4gh/variants/{id}'.format(
            id=query_id
        )

        return self.rest_query(
            method_type,
            endpoint,
            norm_func=norm_func
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def post_ga4gh_variant_annotations_search(self, variant_annotation_set_id,
                                              effects=None, end=None,
                                              page_size=None, page_token=None,
                                              reference_id=None,
                                              reference_name=None, start=None,
                                              norm_func=brc.default_pandas):
        """
        Return variant annotation information in GA4GH format for a region on a
        reference sequence. POST method. Queries:
        `ga4gh/variantannotations/search <https://rest.ensembl.org/documentation/info/gavariantannotations>`_.

        Parameters
        ----------
        variant_annotation_set_id : str
            Return variant annotations data for a specific annotationSet
            (Ensembl is an alias for the current default annotation set).
            Example values: 'Ensembl'.
        effects : [ontologyterm] or NoneType, optional, default: NoneType
            Return variant annotation filtering on effects. Example values: '[
            {"sourceName":"SO", "term":"missense_variant", "id":"SO:0001583",
            "sourceVersion": ""}]'.
        end : int or NoneType, optional, default: NoneType
            End position of region (zero-based, exclusive). Example values:
            '25221500'.
        page_size : int or NoneType, optional, default: NoneType
            Number of variants to show per page.
        page_token : int or NoneType, optional, default: NoneType
            Identifier showing which page of data to retrieve next.
        reference_id : str or NoneType, optional, default: NoneType
            Reference sequence id. Example values:
            '9489ae7581e14efcad134f02afafe26c'.
        reference_name : str or NoneType, optional, default: NoneType
            Reference sequence name. Example values: '22'.
        start : int or NoneType, optional, default: NoneType
            Start position of region (zero-based, inclusive). Example values:
            '25221400'.
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use when obj.pandas is True

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'POST'
        endpoint = 'ga4gh/variantannotations/search'

        return self.rest_query(
            method_type,
            endpoint,
            variantAnnotationSetId=variant_annotation_set_id,
            effects=effects,
            end=end,
            pageSize=page_size,
            pageToken=page_token,
            referenceId=reference_id,
            referenceName=reference_name,
            start=start,
            norm_func=norm_func
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def post_ga4gh_variants_search(self, end, reference_name, start,
                                   variant_set_id, call_set_ids=None,
                                   page_size=None, page_token=None,
                                   norm_func=brc.default_pandas):
        """
        Return variant call information in GA4GH format for a region on a
        reference sequence. POST method. Queries:
        `ga4gh/variants/search <https://rest.ensembl.org/documentation/info/gavariants>`_.

        Parameters
        ----------
        end : int
            End position of region (zero-based, exclusive). Example values:
            '25455087'.
        reference_name : str
            Reference sequence name. Example values: '22'.
        start : int
            Start position of region (zero-based, inclusive). Example values:
            '25455086'.
        variant_set_id : str
            Return variant data for specific variantSets. Example values: '1'.
        call_set_ids : str or NoneType, optional, default: NoneType
            Return variant data for specific callSets. Example values: '[
            1:NA19777 ]'.
        page_size : int or NoneType, optional, default: NoneType
            Number of variants to show per page.
        page_token : int or NoneType, optional, default: NoneType
            Identifier showing which page of data to retrieve next.
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use when obj.pandas is True

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'POST'
        endpoint = 'ga4gh/variants/search'

        return self.rest_query(
            method_type,
            endpoint,
            end=end,
            referenceName=reference_name,
            start=start,
            variantSetId=variant_set_id,
            callSetIds=call_set_ids,
            pageSize=page_size,
            pageToken=page_token,
            norm_func=norm_func
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def post_ga4gh_variantsets_search(self, dataset_id, page_size=None,
                                      page_token=None,
                                      norm_func=brc.default_pandas):
        """
        Return a list of variant sets in GA4GH format. POST method. Queries:
        `ga4gh/variantsets/search <https://rest.ensembl.org/documentation/info/gavariantset>`_.

        Parameters
        ----------
        dataset_id : str
            Return variantSets by dataSet Identifier. Example values:
            '6e340c4d1e333c7a676b1710d2e3953c'.
        page_size : int or NoneType, optional, default: NoneType
            Number of variantSets to return per request.
        page_token : int or NoneType, optional, default: NoneType
            Identifier showing which page of data to retrieve next.
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use when obj.pandas is True

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'POST'
        endpoint = 'ga4gh/variantsets/search'

        return self.rest_query(
            method_type,
            endpoint,
            datasetId=dataset_id,
            pageSize=page_size,
            pageToken=page_token,
            norm_func=norm_func
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_ga4gh_variantsets(self, query_id, norm_func=brc.default_pandas):
        """
        Return the GA4GH record for a specific VariantSet given its identifier.
        GET method. Queries:
        `ga4gh/variantsets/:id <https://rest.ensembl.org/documentation/info/gavariantset_id>`_.

        Parameters
        ----------
        query_id : str
            VariantSet id. Example values: '1'.
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use when obj.pandas is True

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'GET'
        endpoint = 'ga4gh/variantsets/{id}'.format(
            id=query_id
        )

        return self.rest_query(
            method_type,
            endpoint,
            norm_func=norm_func
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def post_ga4gh_references_search(self, reference_set_id, accession=None,
                                     md5checksum=None, page_size=None,
                                     page_token=None,
                                     norm_func=brc.default_pandas):
        """
        Return a list of reference sequences in GA4GH format. POST method.
        Queries:
        `ga4gh/references/search <https://rest.ensembl.org/documentation/info/references>`_.

        Parameters
        ----------
        reference_set_id : str
            Return references for a referenceSet. Example values: 'GRCh38'.
        accession : str or NoneType, optional, default: NoneType
            Return reference information for a specific accession. Example
            values: 'NC_000021.9'.
        md5checksum : str or NoneType, optional, default: NoneType
            Return reference information for the md5checksum of the sequence.
            Example values: '9489ae7581e14efcad134f02afafe26c'.
        page_size : int or NoneType, optional, default: NoneType
            Number of references to return per request.
        page_token : int or NoneType, optional, default: NoneType
            Identifier showing which page of data to retrieve next.
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use when obj.pandas is True

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'POST'
        endpoint = 'ga4gh/references/search'

        return self.rest_query(
            method_type,
            endpoint,
            referenceSetId=reference_set_id,
            accession=accession,
            md5checksum=md5checksum,
            pageSize=page_size,
            pageToken=page_token,
            norm_func=norm_func
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_ga4gh_references(self, query_id, norm_func=brc.default_pandas):
        """
        Return data for a specific reference in GA4GH format by id. GET method.
        Queries:
        `ga4gh/references/:id <https://rest.ensembl.org/documentation/info/references_id>`_.

        Parameters
        ----------
        query_id : str
            Reference id. Example values: '9489ae7581e14efcad134f02afafe26c'.
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use when obj.pandas is True

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'GET'
        endpoint = 'ga4gh/references/{id}'.format(
            id=query_id
        )

        return self.rest_query(
            method_type,
            endpoint,
            norm_func=norm_func
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def post_ga4gh_referencesets_search(self, accession=None, page_size=None,
                                        page_token=None,
                                        norm_func=brc.default_pandas):
        """
        Return a list of reference sets in GA4GH format. POST method. Queries:
        `ga4gh/referencesets/search <https://rest.ensembl.org/documentation/info/referenceSets>`_.

        Parameters
        ----------
        accession : str or NoneType, optional, default: NoneType
            Return referenceSet information for a specific accession. Example
            values: '1'.
        page_size : int or NoneType, optional, default: NoneType
            Number of referenceSets to return per request.
        page_token : int or NoneType, optional, default: NoneType
            Identifier showing which page of data to retrieve next.
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use when obj.pandas is True

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'POST'
        endpoint = 'ga4gh/referencesets/search'

        return self.rest_query(
            method_type,
            endpoint,
            accession=accession,
            pageSize=page_size,
            pageToken=page_token,
            norm_func=norm_func
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_ga4gh_referencesets(self, query_id, norm_func=brc.default_pandas):
        """
        Return data for a specific reference set in GA4GH format. GET method.
        Queries:
        `ga4gh/referencesets/:id <https://rest.ensembl.org/documentation/info/referenceSets_id>`_.

        Parameters
        ----------
        query_id : str
            Reference set id.
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use when obj.pandas is True

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'GET'
        endpoint = 'ga4gh/referencesets/{id}'.format(
            id=query_id
        )

        return self.rest_query(
            method_type,
            endpoint,
            norm_func=norm_func
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def post_ga4gh_variantannotationsets_search(self, variant_set_id,
                                                page_size=None,
                                                page_token=None,
                                                norm_func=brc.default_pandas):
        """
        Return a list of annotation sets in GA4GH format. POST method. Queries:
        `ga4gh/variantannotationsets/search <https://rest.ensembl.org/documentation/info/VariantAnnotationSet>`_.

        Parameters
        ----------
        variant_set_id : str
            Return variant annotation sets for a specific variant set. Example
            values: '1'.
        page_size : int or NoneType, optional, default: NoneType
            Number of variant annotation sets to return per request.
        page_token : int or NoneType, optional, default: NoneType
            Identifier showing which page of data to retrieve next.
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use when obj.pandas is True

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'POST'
        endpoint = 'ga4gh/variantannotationsets/search'

        return self.rest_query(
            method_type,
            endpoint,
            variantSetId=variant_set_id,
            pageSize=page_size,
            pageToken=page_token,
            norm_func=norm_func
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_ga4gh_variantannotationsets(self, query_id,
                                        norm_func=brc.default_pandas):
        """
        Return meta data for a specific annotation set in GA4GH format. GET
        method. Queries:
        `ga4gh/variantannotationsets/:id <https://rest.ensembl.org/documentation/info/VariantAnnotationSet_id>`_.

        Parameters
        ----------
        query_id : str
            VariantAnnotation set id. Example values: 'Ensembl'.
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use when obj.pandas is True

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'GET'
        endpoint = 'ga4gh/variantannotationsets/{id}'.format(
            id=query_id
        )

        return self.rest_query(
            method_type,
            endpoint,
            norm_func=norm_func
        )


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class VariationGa4Gh(brc._BaseRestClient, _VariationGa4Gh):
    """Perform VariationGa4Gh queries
    """
    pass
