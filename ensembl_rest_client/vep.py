"""Perform queries against the variation effect predictor (VEP) endpoints
"""
from ensembl_rest_client.base import base_rest_client as brc


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class _Vep(object):
    """Perform Vep queries
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_vep_hgvs(self, hgvs_notation, species, blosum62=None, cadd=None,
                     conservation=None, gene_splicer=None, lo_f=None,
                     max_ent_scan=None, phenotypes=None, splice_ai=None,
                     splice_region=None, appris=None, canonical=None,
                     ccds=None, db_nsfp=None, dbsc_snv=None, distance=None,
                     domains=None, failed=None, hgvs=None, mane=None,
                     merged=None, mi_rna=None, minimal=None, numbers=None,
                     protein=None, refseq=None, shift_3prime=None,
                     shift_genomic=None, transcript_id=None,
                     transcript_version=None, tsl=None, uniprot=None,
                     variant_class=None, vcf_string=None, xref_refseq=None,
                     norm_func=brc.default_pandas):
        """
        Fetch variant consequences based on a HGVS notation. GET method.
        Queries:
        `vep/:species/hgvs/:hgvs_notation <https://rest.ensembl.org/documentation/info/vep_hgvs_get>`_.

        Parameters
        ----------
        hgvs_notation : str
            HGVS notation. May be genomic (g), coding (c) or protein (p), with
            reference to chromosome name, gene name, transcript ID or protein
            ID. Example values: 'AGT:c.803T>C 9:g.22125504G>C
            ENST00000003084:c.1431_1433delTTC'.
        species : str
            Species name/alias. Example values: 'homo_sapiens human'.
        blosum62 : bool or NoneType, optional, default: NoneType
            Include BLOSUM62 amino acid conservation score (plugin details).
        cadd : bool or NoneType, optional, default: NoneType
            Include CADD (Combined Annotation Dependent Depletion)
            deleteriousness scores for single nucleotide variants. See license.
            (plugin details).
        conservation : bool or NoneType, optional, default: NoneType
            Retrieves a conservation score from the Ensembl Compara databases
            for variant positions (plugin details).
        gene_splicer : bool or NoneType, optional, default: NoneType
            Detects splice sites in genomic DNA (plugin details).
        lo_f : bool or NoneType, optional, default: NoneType
            LOFTEE identifies LoF (loss-of-function) variation. See README for
            more details.
        max_ent_scan : bool or NoneType, optional, default: NoneType
            Sequence motif and maximum entropy based splice site consensus
            predictions (plugin details).
        phenotypes : bool or NoneType, optional, default: NoneType
            Retrieves overlapping phenotype information (plugin details).
        splice_ai : bool or NoneType, optional, default: NoneType
            Retrieves pre-calculated annotations from SpliceAI a deep neural
            network, developed by Illumina, Inc that predicts splice junctions
            from an arbitrary pre-mRNA transcript sequence. Used for non-
            commercial purposes. (plugin details).
        splice_region : bool or NoneType, optional, default: NoneType
            More granular predictions of splicing effects (plugin details).
        appris : bool or NoneType, optional, default: NoneType
            Include APPRIS isoform annotation.
        canonical : bool or NoneType, optional, default: NoneType
            Include a flag indicating the canonical transcript for a gene.
        ccds : bool or NoneType, optional, default: NoneType
            Include CCDS transcript identifiers.
        db_nsfp : str or NoneType, optional, default: NoneType
            Include fields from dbNSFP, a database of pathogenicity predictions
            for missense variants. Multiple fields should be separated by
            commas. See dbNSFP README for field list. (plugin details). Example
            values: 'LRT_pred,MutationTaster_pred'.
        dbsc_snv : bool or NoneType, optional, default: NoneType
            Predictions for splicing variants from dbscSNV. (plugin details).
        distance : int or NoneType, optional, default: NoneType
            Change the distance to transcript for which VEP assigns upstream
            and downstream consequences.
        domains : bool or NoneType, optional, default: NoneType
            Include names of overlapping protein domains.
        failed : bool or NoneType, optional, default: NoneType
            When checking for co-located variants, by default variants flagged
            as failed by Ensembl's QC pipeline will be excluded. Set this flag
            to 1 to include such variants.
        hgvs : bool or NoneType, optional, default: NoneType
            Include HGVS nomenclature based on Ensembl stable identifiers.
        mane : bool or NoneType, optional, default: NoneType
            Include MANE Select annotations (GRCh38 only).
        merged : bool or NoneType, optional, default: NoneType
            Use merged Ensembl and RefSeq transcript set to report consequences
            (human only).
        mi_rna : bool or NoneType, optional, default: NoneType
            Determines where in the secondary structure of a miRNA a variant
            falls (plugin details).
        minimal : bool or NoneType, optional, default: NoneType
            Convert alleles to their most minimal representation before
            consequence calculation i.e. sequence that is identical between
            each pair of reference and alternate alleles is trimmed off from
            both ends, with coordinates adjusted accordingly. Note this may
            lead to discrepancies between input coordinates and coordinates
            reported by VEP relative to transcript sequences.
        numbers : bool or NoneType, optional, default: NoneType
            Include affected exon and intron positions within the transcript.
        protein : bool or NoneType, optional, default: NoneType
            Include Ensembl protein identifiers.
        refseq : bool or NoneType, optional, default: NoneType
            Use RefSeq transcript set to report consequences (human only).
        shift_3prime : bool or NoneType, optional, default: NoneType
            Shift transcript-overlapping variants as far as possible in the 3'
            direction before providing consequences.
        shift_genomic : bool or NoneType, optional, default: NoneType
            Shift all variants as far as possible in the 3' direction before
            providing consequences.
        transcript_id : str or NoneType, optional, default: NoneType
            Filter results by Transcript ID.
        transcript_version : bool or NoneType, optional, default: NoneType
            Add version numbers to Ensembl transcript identifiers.
        tsl : bool or NoneType, optional, default: NoneType
            Include transcript support level (TSL) annotation.
        uniprot : bool or NoneType, optional, default: NoneType
            Include best match accessions for translated protein products from
            three UniProt-related databases (SWISSPROT, TREMBL and UniParc).
        variant_class : bool or NoneType, optional, default: NoneType
            Output the Sequence Ontology variant class for the input variant.
        vcf_string : bool or NoneType, optional, default: NoneType
            Include alleles in VCF format.
        xref_refseq : bool or NoneType, optional, default: NoneType
            Include aligned RefSeq mRNA identifiers for transcript. NB:
            theRefSeq and Ensembl transcripts aligned in this way MAY NOT, AND
            FREQUENTLY WILL NOT, match exactly in sequence, exon structure and
            protein product.
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use when obj.pandas is True

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'GET'
        endpoint = 'vep/{species}/hgvs/{hgvs_notation}'.format(
            species=species,
            hgvs_notation=hgvs_notation
        )

        return self.rest_query(
            method_type,
            endpoint,
            Blosum62=blosum62,
            CADD=cadd,
            Conservation=conservation,
            GeneSplicer=gene_splicer,
            LoF=lo_f,
            MaxEntScan=max_ent_scan,
            Phenotypes=phenotypes,
            SpliceAI=splice_ai,
            SpliceRegion=splice_region,
            appris=appris,
            canonical=canonical,
            ccds=ccds,
            dbNSFP=db_nsfp,
            dbscSNV=dbsc_snv,
            distance=distance,
            domains=domains,
            failed=failed,
            hgvs=hgvs,
            mane=mane,
            merged=merged,
            miRNA=mi_rna,
            minimal=minimal,
            numbers=numbers,
            protein=protein,
            refseq=refseq,
            shift_3prime=shift_3prime,
            shift_genomic=shift_genomic,
            transcript_id=transcript_id,
            transcript_version=transcript_version,
            tsl=tsl,
            uniprot=uniprot,
            variant_class=variant_class,
            vcf_string=vcf_string,
            xref_refseq=xref_refseq,
            norm_func=norm_func
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def post_vep_hgvs(self, species, hgvs_notations, blosum62=None, cadd=None,
                      gene_splicer=None, lo_f=None, max_ent_scan=None,
                      phenotypes=None, splice_ai=None, splice_region=None,
                      appris=None, canonical=None, ccds=None, db_nsfp=None,
                      dbsc_snv=None, distance=None, domains=None, failed=None,
                      hgvs=None, mane=None, merged=None, mi_rna=None,
                      minimal=None, numbers=None, protein=None, refseq=None,
                      shift_3prime=None, shift_genomic=None,
                      transcript_id=None, transcript_version=None, tsl=None,
                      uniprot=None, variant_class=None, vcf_string=None,
                      xref_refseq=None, norm_func=brc.default_pandas):
        """
        Fetch variant consequences for multiple HGVS notations. POST method.
        Queries:
        `vep/:species/hgvs <https://rest.ensembl.org/documentation/info/vep_hgvs_post>`_.
        Max post size: 200.

        Parameters
        ----------
        species : str
            Species name/alias. Example values: 'homo_sapiens human'.
        hgvs_notations : list
            Post option not defined in required/optional args.
        blosum62 : bool or NoneType, optional, default: NoneType
            Include BLOSUM62 amino acid conservation score (plugin details).
        cadd : bool or NoneType, optional, default: NoneType
            Include CADD (Combined Annotation Dependent Depletion)
            deleteriousness scores for single nucleotide variants. See license.
            (plugin details).
        gene_splicer : bool or NoneType, optional, default: NoneType
            Detects splice sites in genomic DNA (plugin details).
        lo_f : bool or NoneType, optional, default: NoneType
            LOFTEE identifies LoF (loss-of-function) variation. See README for
            more details.
        max_ent_scan : bool or NoneType, optional, default: NoneType
            Sequence motif and maximum entropy based splice site consensus
            predictions (plugin details).
        phenotypes : bool or NoneType, optional, default: NoneType
            Retrieves overlapping phenotype information (plugin details).
        splice_ai : bool or NoneType, optional, default: NoneType
            Retrieves pre-calculated annotations from SpliceAI a deep neural
            network, developed by Illumina, Inc that predicts splice junctions
            from an arbitrary pre-mRNA transcript sequence. Used for non-
            commercial purposes. (plugin details).
        splice_region : bool or NoneType, optional, default: NoneType
            More granular predictions of splicing effects (plugin details).
        appris : bool or NoneType, optional, default: NoneType
            Include APPRIS isoform annotation.
        canonical : bool or NoneType, optional, default: NoneType
            Include a flag indicating the canonical transcript for a gene.
        ccds : bool or NoneType, optional, default: NoneType
            Include CCDS transcript identifiers.
        db_nsfp : str or NoneType, optional, default: NoneType
            Include fields from dbNSFP, a database of pathogenicity predictions
            for missense variants. Multiple fields should be separated by
            commas. See dbNSFP README for field list. (plugin details). Example
            values: 'LRT_pred,MutationTaster_pred'.
        dbsc_snv : bool or NoneType, optional, default: NoneType
            Predictions for splicing variants from dbscSNV. (plugin details).
        distance : int or NoneType, optional, default: NoneType
            Change the distance to transcript for which VEP assigns upstream
            and downstream consequences.
        domains : bool or NoneType, optional, default: NoneType
            Include names of overlapping protein domains.
        failed : bool or NoneType, optional, default: NoneType
            When checking for co-located variants, by default variants flagged
            as failed by Ensembl's QC pipeline will be excluded. Set this flag
            to 1 to include such variants.
        hgvs : bool or NoneType, optional, default: NoneType
            Include HGVS nomenclature based on Ensembl stable identifiers.
        mane : bool or NoneType, optional, default: NoneType
            Include MANE Select annotations (GRCh38 only).
        merged : bool or NoneType, optional, default: NoneType
            Use merged Ensembl and RefSeq transcript set to report consequences
            (human only).
        mi_rna : bool or NoneType, optional, default: NoneType
            Determines where in the secondary structure of a miRNA a variant
            falls (plugin details).
        minimal : bool or NoneType, optional, default: NoneType
            Convert alleles to their most minimal representation before
            consequence calculation i.e. sequence that is identical between
            each pair of reference and alternate alleles is trimmed off from
            both ends, with coordinates adjusted accordingly. Note this may
            lead to discrepancies between input coordinates and coordinates
            reported by VEP relative to transcript sequences.
        numbers : bool or NoneType, optional, default: NoneType
            Include affected exon and intron positions within the transcript.
        protein : bool or NoneType, optional, default: NoneType
            Include Ensembl protein identifiers.
        refseq : bool or NoneType, optional, default: NoneType
            Use RefSeq transcript set to report consequences (human only).
        shift_3prime : bool or NoneType, optional, default: NoneType
            Shift transcript-overlapping variants as far as possible in the 3'
            direction before providing consequences.
        shift_genomic : bool or NoneType, optional, default: NoneType
            Shift all variants as far as possible in the 3' direction before
            providing consequences.
        transcript_id : str or NoneType, optional, default: NoneType
            Filter results by Transcript ID.
        transcript_version : bool or NoneType, optional, default: NoneType
            Add version numbers to Ensembl transcript identifiers.
        tsl : bool or NoneType, optional, default: NoneType
            Include transcript support level (TSL) annotation.
        uniprot : bool or NoneType, optional, default: NoneType
            Include best match accessions for translated protein products from
            three UniProt-related databases (SWISSPROT, TREMBL and UniParc).
        variant_class : bool or NoneType, optional, default: NoneType
            Output the Sequence Ontology variant class for the input variant.
        vcf_string : bool or NoneType, optional, default: NoneType
            Include alleles in VCF format.
        xref_refseq : bool or NoneType, optional, default: NoneType
            Include aligned RefSeq mRNA identifiers for transcript. NB:
            theRefSeq and Ensembl transcripts aligned in this way MAY NOT, AND
            FREQUENTLY WILL NOT, match exactly in sequence, exon structure and
            protein product.
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use when obj.pandas is True

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'POST'
        maximum_post_size = 200
        endpoint = 'vep/{species}/hgvs'.format(
            species=species
        )

        return self.rest_query(
            method_type,
            endpoint,
            maximum_post_size=maximum_post_size,
            hgvs_notations=hgvs_notations,
            Blosum62=blosum62,
            CADD=cadd,
            GeneSplicer=gene_splicer,
            LoF=lo_f,
            MaxEntScan=max_ent_scan,
            Phenotypes=phenotypes,
            SpliceAI=splice_ai,
            SpliceRegion=splice_region,
            appris=appris,
            canonical=canonical,
            ccds=ccds,
            dbNSFP=db_nsfp,
            dbscSNV=dbsc_snv,
            distance=distance,
            domains=domains,
            failed=failed,
            hgvs=hgvs,
            mane=mane,
            merged=merged,
            miRNA=mi_rna,
            minimal=minimal,
            numbers=numbers,
            protein=protein,
            refseq=refseq,
            shift_3prime=shift_3prime,
            shift_genomic=shift_genomic,
            transcript_id=transcript_id,
            transcript_version=transcript_version,
            tsl=tsl,
            uniprot=uniprot,
            variant_class=variant_class,
            vcf_string=vcf_string,
            xref_refseq=xref_refseq,
            norm_func=norm_func
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_vep_id(self, query_id, species, blosum62=None, cadd=None,
                   conservation=None, gene_splicer=None, lo_f=None,
                   max_ent_scan=None, phenotypes=None, splice_ai=None,
                   splice_region=None, appris=None, canonical=None, ccds=None,
                   db_nsfp=None, dbsc_snv=None, distance=None, domains=None,
                   failed=None, hgvs=None, mane=None, merged=None, mi_rna=None,
                   minimal=None, numbers=None, protein=None, refseq=None,
                   shift_3prime=None, shift_genomic=None, transcript_id=None,
                   transcript_version=None, tsl=None, uniprot=None,
                   variant_class=None, vcf_string=None, xref_refseq=None,
                   norm_func=brc.default_pandas):
        """
        Fetch variant consequences based on a variant identifier. GET method.
        Queries:
        `vep/:species/id/:id <https://rest.ensembl.org/documentation/info/vep_id_get>`_.

        Parameters
        ----------
        query_id : str
            Query ID. Supports dbSNP, COSMIC and HGMD identifiers. Example
            values: 'rs56116432 COSM476'.
        species : str
            Species name/alias. Example values: 'homo_sapiens human'.
        blosum62 : bool or NoneType, optional, default: NoneType
            Include BLOSUM62 amino acid conservation score (plugin details).
        cadd : bool or NoneType, optional, default: NoneType
            Include CADD (Combined Annotation Dependent Depletion)
            deleteriousness scores for single nucleotide variants. See license.
            (plugin details).
        conservation : bool or NoneType, optional, default: NoneType
            Retrieves a conservation score from the Ensembl Compara databases
            for variant positions (plugin details).
        gene_splicer : bool or NoneType, optional, default: NoneType
            Detects splice sites in genomic DNA (plugin details).
        lo_f : bool or NoneType, optional, default: NoneType
            LOFTEE identifies LoF (loss-of-function) variation. See README for
            more details.
        max_ent_scan : bool or NoneType, optional, default: NoneType
            Sequence motif and maximum entropy based splice site consensus
            predictions (plugin details).
        phenotypes : bool or NoneType, optional, default: NoneType
            Retrieves overlapping phenotype information (plugin details).
        splice_ai : bool or NoneType, optional, default: NoneType
            Retrieves pre-calculated annotations from SpliceAI a deep neural
            network, developed by Illumina, Inc that predicts splice junctions
            from an arbitrary pre-mRNA transcript sequence. Used for non-
            commercial purposes. (plugin details).
        splice_region : bool or NoneType, optional, default: NoneType
            More granular predictions of splicing effects (plugin details).
        appris : bool or NoneType, optional, default: NoneType
            Include APPRIS isoform annotation.
        canonical : bool or NoneType, optional, default: NoneType
            Include a flag indicating the canonical transcript for a gene.
        ccds : bool or NoneType, optional, default: NoneType
            Include CCDS transcript identifiers.
        db_nsfp : str or NoneType, optional, default: NoneType
            Include fields from dbNSFP, a database of pathogenicity predictions
            for missense variants. Multiple fields should be separated by
            commas. See dbNSFP README for field list. (plugin details). Example
            values: 'LRT_pred,MutationTaster_pred'.
        dbsc_snv : bool or NoneType, optional, default: NoneType
            Predictions for splicing variants from dbscSNV. (plugin details).
        distance : int or NoneType, optional, default: NoneType
            Change the distance to transcript for which VEP assigns upstream
            and downstream consequences.
        domains : bool or NoneType, optional, default: NoneType
            Include names of overlapping protein domains.
        failed : bool or NoneType, optional, default: NoneType
            When checking for co-located variants, by default variants flagged
            as failed by Ensembl's QC pipeline will be excluded. Set this flag
            to 1 to include such variants.
        hgvs : bool or NoneType, optional, default: NoneType
            Include HGVS nomenclature based on Ensembl stable identifiers.
        mane : bool or NoneType, optional, default: NoneType
            Include MANE Select annotations (GRCh38 only).
        merged : bool or NoneType, optional, default: NoneType
            Use merged Ensembl and RefSeq transcript set to report consequences
            (human only).
        mi_rna : bool or NoneType, optional, default: NoneType
            Determines where in the secondary structure of a miRNA a variant
            falls (plugin details).
        minimal : bool or NoneType, optional, default: NoneType
            Convert alleles to their most minimal representation before
            consequence calculation i.e. sequence that is identical between
            each pair of reference and alternate alleles is trimmed off from
            both ends, with coordinates adjusted accordingly. Note this may
            lead to discrepancies between input coordinates and coordinates
            reported by VEP relative to transcript sequences.
        numbers : bool or NoneType, optional, default: NoneType
            Include affected exon and intron positions within the transcript.
        protein : bool or NoneType, optional, default: NoneType
            Include Ensembl protein identifiers.
        refseq : bool or NoneType, optional, default: NoneType
            Use RefSeq transcript set to report consequences (human only).
        shift_3prime : bool or NoneType, optional, default: NoneType
            Shift transcript-overlapping variants as far as possible in the 3'
            direction before providing consequences.
        shift_genomic : bool or NoneType, optional, default: NoneType
            Shift all variants as far as possible in the 3' direction before
            providing consequences.
        transcript_id : str or NoneType, optional, default: NoneType
            Filter results by Transcript ID.
        transcript_version : bool or NoneType, optional, default: NoneType
            Add version numbers to Ensembl transcript identifiers.
        tsl : bool or NoneType, optional, default: NoneType
            Include transcript support level (TSL) annotation.
        uniprot : bool or NoneType, optional, default: NoneType
            Include best match accessions for translated protein products from
            three UniProt-related databases (SWISSPROT, TREMBL and UniParc).
        variant_class : bool or NoneType, optional, default: NoneType
            Output the Sequence Ontology variant class for the input variant.
        vcf_string : bool or NoneType, optional, default: NoneType
            Include alleles in VCF format.
        xref_refseq : bool or NoneType, optional, default: NoneType
            Include aligned RefSeq mRNA identifiers for transcript. NB:
            theRefSeq and Ensembl transcripts aligned in this way MAY NOT, AND
            FREQUENTLY WILL NOT, match exactly in sequence, exon structure and
            protein product.
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use when obj.pandas is True

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'GET'
        endpoint = 'vep/{species}/id/{id}'.format(
            species=species,
            id=query_id
        )

        return self.rest_query(
            method_type,
            endpoint,
            Blosum62=blosum62,
            CADD=cadd,
            Conservation=conservation,
            GeneSplicer=gene_splicer,
            LoF=lo_f,
            MaxEntScan=max_ent_scan,
            Phenotypes=phenotypes,
            SpliceAI=splice_ai,
            SpliceRegion=splice_region,
            appris=appris,
            canonical=canonical,
            ccds=ccds,
            dbNSFP=db_nsfp,
            dbscSNV=dbsc_snv,
            distance=distance,
            domains=domains,
            failed=failed,
            hgvs=hgvs,
            mane=mane,
            merged=merged,
            miRNA=mi_rna,
            minimal=minimal,
            numbers=numbers,
            protein=protein,
            refseq=refseq,
            shift_3prime=shift_3prime,
            shift_genomic=shift_genomic,
            transcript_id=transcript_id,
            transcript_version=transcript_version,
            tsl=tsl,
            uniprot=uniprot,
            variant_class=variant_class,
            vcf_string=vcf_string,
            xref_refseq=xref_refseq,
            norm_func=norm_func
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def post_vep_id(self, species, ids, blosum62=None, cadd=None,
                    gene_splicer=None, lo_f=None, max_ent_scan=None,
                    phenotypes=None, splice_ai=None, splice_region=None,
                    appris=None, canonical=None, ccds=None, db_nsfp=None,
                    dbsc_snv=None, distance=None, domains=None, failed=None,
                    hgvs=None, mane=None, merged=None, mi_rna=None,
                    minimal=None, numbers=None, protein=None, refseq=None,
                    shift_3prime=None, shift_genomic=None, transcript_id=None,
                    transcript_version=None, tsl=None, uniprot=None,
                    variant_class=None, vcf_string=None, xref_refseq=None,
                    norm_func=brc.default_pandas):
        """
        Fetch variant consequences for multiple ids. POST method. Queries:
        `vep/:species/id <https://rest.ensembl.org/documentation/info/vep_id_post>`_.
        Max post size: 200.

        Parameters
        ----------
        species : str
            Species name/alias. Example values: 'homo_sapiens human'.
        ids : list
            Post option not defined in required/optional args.
        blosum62 : bool or NoneType, optional, default: NoneType
            Include BLOSUM62 amino acid conservation score (plugin details).
        cadd : bool or NoneType, optional, default: NoneType
            Include CADD (Combined Annotation Dependent Depletion)
            deleteriousness scores for single nucleotide variants. See license.
            (plugin details).
        gene_splicer : bool or NoneType, optional, default: NoneType
            Detects splice sites in genomic DNA (plugin details).
        lo_f : bool or NoneType, optional, default: NoneType
            LOFTEE identifies LoF (loss-of-function) variation. See README for
            more details.
        max_ent_scan : bool or NoneType, optional, default: NoneType
            Sequence motif and maximum entropy based splice site consensus
            predictions (plugin details).
        phenotypes : bool or NoneType, optional, default: NoneType
            Retrieves overlapping phenotype information (plugin details).
        splice_ai : bool or NoneType, optional, default: NoneType
            Retrieves pre-calculated annotations from SpliceAI a deep neural
            network, developed by Illumina, Inc that predicts splice junctions
            from an arbitrary pre-mRNA transcript sequence. Used for non-
            commercial purposes. (plugin details).
        splice_region : bool or NoneType, optional, default: NoneType
            More granular predictions of splicing effects (plugin details).
        appris : bool or NoneType, optional, default: NoneType
            Include APPRIS isoform annotation.
        canonical : bool or NoneType, optional, default: NoneType
            Include a flag indicating the canonical transcript for a gene.
        ccds : bool or NoneType, optional, default: NoneType
            Include CCDS transcript identifiers.
        db_nsfp : str or NoneType, optional, default: NoneType
            Include fields from dbNSFP, a database of pathogenicity predictions
            for missense variants. Multiple fields should be separated by
            commas. See dbNSFP README for field list. (plugin details). Example
            values: 'LRT_pred,MutationTaster_pred'.
        dbsc_snv : bool or NoneType, optional, default: NoneType
            Predictions for splicing variants from dbscSNV. (plugin details).
        distance : int or NoneType, optional, default: NoneType
            Change the distance to transcript for which VEP assigns upstream
            and downstream consequences.
        domains : bool or NoneType, optional, default: NoneType
            Include names of overlapping protein domains.
        failed : bool or NoneType, optional, default: NoneType
            When checking for co-located variants, by default variants flagged
            as failed by Ensembl's QC pipeline will be excluded. Set this flag
            to 1 to include such variants.
        hgvs : bool or NoneType, optional, default: NoneType
            Include HGVS nomenclature based on Ensembl stable identifiers.
        mane : bool or NoneType, optional, default: NoneType
            Include MANE Select annotations (GRCh38 only).
        merged : bool or NoneType, optional, default: NoneType
            Use merged Ensembl and RefSeq transcript set to report consequences
            (human only).
        mi_rna : bool or NoneType, optional, default: NoneType
            Determines where in the secondary structure of a miRNA a variant
            falls (plugin details).
        minimal : bool or NoneType, optional, default: NoneType
            Convert alleles to their most minimal representation before
            consequence calculation i.e. sequence that is identical between
            each pair of reference and alternate alleles is trimmed off from
            both ends, with coordinates adjusted accordingly. Note this may
            lead to discrepancies between input coordinates and coordinates
            reported by VEP relative to transcript sequences.
        numbers : bool or NoneType, optional, default: NoneType
            Include affected exon and intron positions within the transcript.
        protein : bool or NoneType, optional, default: NoneType
            Include Ensembl protein identifiers.
        refseq : bool or NoneType, optional, default: NoneType
            Use RefSeq transcript set to report consequences (human only).
        shift_3prime : bool or NoneType, optional, default: NoneType
            Shift transcript-overlapping variants as far as possible in the 3'
            direction before providing consequences.
        shift_genomic : bool or NoneType, optional, default: NoneType
            Shift all variants as far as possible in the 3' direction before
            providing consequences.
        transcript_id : str or NoneType, optional, default: NoneType
            Filter results by Transcript ID.
        transcript_version : bool or NoneType, optional, default: NoneType
            Add version numbers to Ensembl transcript identifiers.
        tsl : bool or NoneType, optional, default: NoneType
            Include transcript support level (TSL) annotation.
        uniprot : bool or NoneType, optional, default: NoneType
            Include best match accessions for translated protein products from
            three UniProt-related databases (SWISSPROT, TREMBL and UniParc).
        variant_class : bool or NoneType, optional, default: NoneType
            Output the Sequence Ontology variant class for the input variant.
        vcf_string : bool or NoneType, optional, default: NoneType
            Include alleles in VCF format.
        xref_refseq : bool or NoneType, optional, default: NoneType
            Include aligned RefSeq mRNA identifiers for transcript. NB:
            theRefSeq and Ensembl transcripts aligned in this way MAY NOT, AND
            FREQUENTLY WILL NOT, match exactly in sequence, exon structure and
            protein product.
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use when obj.pandas is True

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'POST'
        maximum_post_size = 200
        endpoint = 'vep/{species}/id'.format(
            species=species
        )

        return self.rest_query(
            method_type,
            endpoint,
            maximum_post_size=maximum_post_size,
            ids=ids,
            Blosum62=blosum62,
            CADD=cadd,
            GeneSplicer=gene_splicer,
            LoF=lo_f,
            MaxEntScan=max_ent_scan,
            Phenotypes=phenotypes,
            SpliceAI=splice_ai,
            SpliceRegion=splice_region,
            appris=appris,
            canonical=canonical,
            ccds=ccds,
            dbNSFP=db_nsfp,
            dbscSNV=dbsc_snv,
            distance=distance,
            domains=domains,
            failed=failed,
            hgvs=hgvs,
            mane=mane,
            merged=merged,
            miRNA=mi_rna,
            minimal=minimal,
            numbers=numbers,
            protein=protein,
            refseq=refseq,
            shift_3prime=shift_3prime,
            shift_genomic=shift_genomic,
            transcript_id=transcript_id,
            transcript_version=transcript_version,
            tsl=tsl,
            uniprot=uniprot,
            variant_class=variant_class,
            vcf_string=vcf_string,
            xref_refseq=xref_refseq,
            norm_func=norm_func
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_vep_region(self, allele, region, species, blosum62=None, cadd=None,
                       conservation=None, gene_splicer=None, lo_f=None,
                       max_ent_scan=None, phenotypes=None, splice_ai=None,
                       splice_region=None, appris=None, canonical=None,
                       ccds=None, db_nsfp=None, dbsc_snv=None, distance=None,
                       domains=None, failed=None, hgvs=None, mane=None,
                       merged=None, mi_rna=None, minimal=None, numbers=None,
                       protein=None, refseq=None, shift_3prime=None,
                       shift_genomic=None, transcript_id=None,
                       transcript_version=None, tsl=None, uniprot=None,
                       variant_class=None, vcf_string=None, xref_refseq=None,
                       norm_func=brc.default_pandas):
        """
        Fetch variant consequences. GET method. Queries:
        `vep/:species/region/:region/:allele/ <https://rest.ensembl.org/documentation/info/vep_region_get>`_.

        Parameters
        ----------
        allele : str
            Variation allele. Example values: 'C DUP'.
        region : str
            Query region. We only support the current assembly. Example values:
            '9:22125503-22125502:1 7:100318423-100321323:1'.
        species : str
            Species name/alias. Example values: 'homo_sapiens human'.
        blosum62 : bool or NoneType, optional, default: NoneType
            Include BLOSUM62 amino acid conservation score (plugin details).
        cadd : bool or NoneType, optional, default: NoneType
            Include CADD (Combined Annotation Dependent Depletion)
            deleteriousness scores for single nucleotide variants. See license.
            (plugin details).
        conservation : bool or NoneType, optional, default: NoneType
            Retrieves a conservation score from the Ensembl Compara databases
            for variant positions (plugin details).
        gene_splicer : bool or NoneType, optional, default: NoneType
            Detects splice sites in genomic DNA (plugin details).
        lo_f : bool or NoneType, optional, default: NoneType
            LOFTEE identifies LoF (loss-of-function) variation. See README for
            more details.
        max_ent_scan : bool or NoneType, optional, default: NoneType
            Sequence motif and maximum entropy based splice site consensus
            predictions (plugin details).
        phenotypes : bool or NoneType, optional, default: NoneType
            Retrieves overlapping phenotype information (plugin details).
        splice_ai : bool or NoneType, optional, default: NoneType
            Retrieves pre-calculated annotations from SpliceAI a deep neural
            network, developed by Illumina, Inc that predicts splice junctions
            from an arbitrary pre-mRNA transcript sequence. Used for non-
            commercial purposes. (plugin details).
        splice_region : bool or NoneType, optional, default: NoneType
            More granular predictions of splicing effects (plugin details).
        appris : bool or NoneType, optional, default: NoneType
            Include APPRIS isoform annotation.
        canonical : bool or NoneType, optional, default: NoneType
            Include a flag indicating the canonical transcript for a gene.
        ccds : bool or NoneType, optional, default: NoneType
            Include CCDS transcript identifiers.
        db_nsfp : str or NoneType, optional, default: NoneType
            Include fields from dbNSFP, a database of pathogenicity predictions
            for missense variants. Multiple fields should be separated by
            commas. See dbNSFP README for field list. (plugin details). Example
            values: 'LRT_pred,MutationTaster_pred'.
        dbsc_snv : bool or NoneType, optional, default: NoneType
            Predictions for splicing variants from dbscSNV. (plugin details).
        distance : int or NoneType, optional, default: NoneType
            Change the distance to transcript for which VEP assigns upstream
            and downstream consequences.
        domains : bool or NoneType, optional, default: NoneType
            Include names of overlapping protein domains.
        failed : bool or NoneType, optional, default: NoneType
            When checking for co-located variants, by default variants flagged
            as failed by Ensembl's QC pipeline will be excluded. Set this flag
            to 1 to include such variants.
        hgvs : bool or NoneType, optional, default: NoneType
            Include HGVS nomenclature based on Ensembl stable identifiers.
        mane : bool or NoneType, optional, default: NoneType
            Include MANE Select annotations (GRCh38 only).
        merged : bool or NoneType, optional, default: NoneType
            Use merged Ensembl and RefSeq transcript set to report consequences
            (human only).
        mi_rna : bool or NoneType, optional, default: NoneType
            Determines where in the secondary structure of a miRNA a variant
            falls (plugin details).
        minimal : bool or NoneType, optional, default: NoneType
            Convert alleles to their most minimal representation before
            consequence calculation i.e. sequence that is identical between
            each pair of reference and alternate alleles is trimmed off from
            both ends, with coordinates adjusted accordingly. Note this may
            lead to discrepancies between input coordinates and coordinates
            reported by VEP relative to transcript sequences.
        numbers : bool or NoneType, optional, default: NoneType
            Include affected exon and intron positions within the transcript.
        protein : bool or NoneType, optional, default: NoneType
            Include Ensembl protein identifiers.
        refseq : bool or NoneType, optional, default: NoneType
            Use RefSeq transcript set to report consequences (human only).
        shift_3prime : bool or NoneType, optional, default: NoneType
            Shift transcript-overlapping variants as far as possible in the 3'
            direction before providing consequences.
        shift_genomic : bool or NoneType, optional, default: NoneType
            Shift all variants as far as possible in the 3' direction before
            providing consequences.
        transcript_id : str or NoneType, optional, default: NoneType
            Filter results by Transcript ID.
        transcript_version : bool or NoneType, optional, default: NoneType
            Add version numbers to Ensembl transcript identifiers.
        tsl : bool or NoneType, optional, default: NoneType
            Include transcript support level (TSL) annotation.
        uniprot : bool or NoneType, optional, default: NoneType
            Include best match accessions for translated protein products from
            three UniProt-related databases (SWISSPROT, TREMBL and UniParc).
        variant_class : bool or NoneType, optional, default: NoneType
            Output the Sequence Ontology variant class for the input variant.
        vcf_string : bool or NoneType, optional, default: NoneType
            Include alleles in VCF format.
        xref_refseq : bool or NoneType, optional, default: NoneType
            Include aligned RefSeq mRNA identifiers for transcript. NB:
            theRefSeq and Ensembl transcripts aligned in this way MAY NOT, AND
            FREQUENTLY WILL NOT, match exactly in sequence, exon structure and
            protein product.
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use when obj.pandas is True

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'GET'
        endpoint = 'vep/{species}/region/{region}/{allele}/'.format(
            species=species,
            region=region,
            allele=allele
        )

        return self.rest_query(
            method_type,
            endpoint,
            Blosum62=blosum62,
            CADD=cadd,
            Conservation=conservation,
            GeneSplicer=gene_splicer,
            LoF=lo_f,
            MaxEntScan=max_ent_scan,
            Phenotypes=phenotypes,
            SpliceAI=splice_ai,
            SpliceRegion=splice_region,
            appris=appris,
            canonical=canonical,
            ccds=ccds,
            dbNSFP=db_nsfp,
            dbscSNV=dbsc_snv,
            distance=distance,
            domains=domains,
            failed=failed,
            hgvs=hgvs,
            mane=mane,
            merged=merged,
            miRNA=mi_rna,
            minimal=minimal,
            numbers=numbers,
            protein=protein,
            refseq=refseq,
            shift_3prime=shift_3prime,
            shift_genomic=shift_genomic,
            transcript_id=transcript_id,
            transcript_version=transcript_version,
            tsl=tsl,
            uniprot=uniprot,
            variant_class=variant_class,
            vcf_string=vcf_string,
            xref_refseq=xref_refseq,
            norm_func=norm_func
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def post_vep_region(self, species, variants, blosum62=None, cadd=None,
                        gene_splicer=None, lo_f=None, max_ent_scan=None,
                        phenotypes=None, splice_ai=None, splice_region=None,
                        appris=None, canonical=None, ccds=None, db_nsfp=None,
                        dbsc_snv=None, distance=None, domains=None,
                        failed=None, hgvs=None, mane=None, merged=None,
                        mi_rna=None, minimal=None, numbers=None, protein=None,
                        refseq=None, shift_3prime=None, shift_genomic=None,
                        transcript_id=None, transcript_version=None, tsl=None,
                        uniprot=None, variant_class=None, vcf_string=None,
                        xref_refseq=None, norm_func=brc.default_pandas):
        """
        Fetch variant consequences for multiple regions. POST method. Queries:
        `vep/:species/region <https://rest.ensembl.org/documentation/info/vep_region_post>`_.
        Max post size: 200.

        Parameters
        ----------
        species : str
            Species name/alias. Example values: 'homo_sapiens human'.
        variants : list
            Post option not defined in required/optional args.
        blosum62 : bool or NoneType, optional, default: NoneType
            Include BLOSUM62 amino acid conservation score (plugin details).
        cadd : bool or NoneType, optional, default: NoneType
            Include CADD (Combined Annotation Dependent Depletion)
            deleteriousness scores for single nucleotide variants. See license.
            (plugin details).
        gene_splicer : bool or NoneType, optional, default: NoneType
            Detects splice sites in genomic DNA (plugin details).
        lo_f : bool or NoneType, optional, default: NoneType
            LOFTEE identifies LoF (loss-of-function) variation. See README for
            more details.
        max_ent_scan : bool or NoneType, optional, default: NoneType
            Sequence motif and maximum entropy based splice site consensus
            predictions (plugin details).
        phenotypes : bool or NoneType, optional, default: NoneType
            Retrieves overlapping phenotype information (plugin details).
        splice_ai : bool or NoneType, optional, default: NoneType
            Retrieves pre-calculated annotations from SpliceAI a deep neural
            network, developed by Illumina, Inc that predicts splice junctions
            from an arbitrary pre-mRNA transcript sequence. Used for non-
            commercial purposes. (plugin details).
        splice_region : bool or NoneType, optional, default: NoneType
            More granular predictions of splicing effects (plugin details).
        appris : bool or NoneType, optional, default: NoneType
            Include APPRIS isoform annotation.
        canonical : bool or NoneType, optional, default: NoneType
            Include a flag indicating the canonical transcript for a gene.
        ccds : bool or NoneType, optional, default: NoneType
            Include CCDS transcript identifiers.
        db_nsfp : str or NoneType, optional, default: NoneType
            Include fields from dbNSFP, a database of pathogenicity predictions
            for missense variants. Multiple fields should be separated by
            commas. See dbNSFP README for field list. (plugin details). Example
            values: 'LRT_pred,MutationTaster_pred'.
        dbsc_snv : bool or NoneType, optional, default: NoneType
            Predictions for splicing variants from dbscSNV. (plugin details).
        distance : int or NoneType, optional, default: NoneType
            Change the distance to transcript for which VEP assigns upstream
            and downstream consequences.
        domains : bool or NoneType, optional, default: NoneType
            Include names of overlapping protein domains.
        failed : bool or NoneType, optional, default: NoneType
            When checking for co-located variants, by default variants flagged
            as failed by Ensembl's QC pipeline will be excluded. Set this flag
            to 1 to include such variants.
        hgvs : bool or NoneType, optional, default: NoneType
            Include HGVS nomenclature based on Ensembl stable identifiers.
        mane : bool or NoneType, optional, default: NoneType
            Include MANE Select annotations (GRCh38 only).
        merged : bool or NoneType, optional, default: NoneType
            Use merged Ensembl and RefSeq transcript set to report consequences
            (human only).
        mi_rna : bool or NoneType, optional, default: NoneType
            Determines where in the secondary structure of a miRNA a variant
            falls (plugin details).
        minimal : bool or NoneType, optional, default: NoneType
            Convert alleles to their most minimal representation before
            consequence calculation i.e. sequence that is identical between
            each pair of reference and alternate alleles is trimmed off from
            both ends, with coordinates adjusted accordingly. Note this may
            lead to discrepancies between input coordinates and coordinates
            reported by VEP relative to transcript sequences.
        numbers : bool or NoneType, optional, default: NoneType
            Include affected exon and intron positions within the transcript.
        protein : bool or NoneType, optional, default: NoneType
            Include Ensembl protein identifiers.
        refseq : bool or NoneType, optional, default: NoneType
            Use RefSeq transcript set to report consequences (human only).
        shift_3prime : bool or NoneType, optional, default: NoneType
            Shift transcript-overlapping variants as far as possible in the 3'
            direction before providing consequences.
        shift_genomic : bool or NoneType, optional, default: NoneType
            Shift all variants as far as possible in the 3' direction before
            providing consequences.
        transcript_id : str or NoneType, optional, default: NoneType
            Filter results by Transcript ID.
        transcript_version : bool or NoneType, optional, default: NoneType
            Add version numbers to Ensembl transcript identifiers.
        tsl : bool or NoneType, optional, default: NoneType
            Include transcript support level (TSL) annotation.
        uniprot : bool or NoneType, optional, default: NoneType
            Include best match accessions for translated protein products from
            three UniProt-related databases (SWISSPROT, TREMBL and UniParc).
        variant_class : bool or NoneType, optional, default: NoneType
            Output the Sequence Ontology variant class for the input variant.
        vcf_string : bool or NoneType, optional, default: NoneType
            Include alleles in VCF format.
        xref_refseq : bool or NoneType, optional, default: NoneType
            Include aligned RefSeq mRNA identifiers for transcript. NB:
            theRefSeq and Ensembl transcripts aligned in this way MAY NOT, AND
            FREQUENTLY WILL NOT, match exactly in sequence, exon structure and
            protein product.
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use when obj.pandas is True

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'POST'
        maximum_post_size = 200
        endpoint = 'vep/{species}/region'.format(
            species=species
        )

        return self.rest_query(
            method_type,
            endpoint,
            maximum_post_size=maximum_post_size,
            variants=variants,
            Blosum62=blosum62,
            CADD=cadd,
            GeneSplicer=gene_splicer,
            LoF=lo_f,
            MaxEntScan=max_ent_scan,
            Phenotypes=phenotypes,
            SpliceAI=splice_ai,
            SpliceRegion=splice_region,
            appris=appris,
            canonical=canonical,
            ccds=ccds,
            dbNSFP=db_nsfp,
            dbscSNV=dbsc_snv,
            distance=distance,
            domains=domains,
            failed=failed,
            hgvs=hgvs,
            mane=mane,
            merged=merged,
            miRNA=mi_rna,
            minimal=minimal,
            numbers=numbers,
            protein=protein,
            refseq=refseq,
            shift_3prime=shift_3prime,
            shift_genomic=shift_genomic,
            transcript_id=transcript_id,
            transcript_version=transcript_version,
            tsl=tsl,
            uniprot=uniprot,
            variant_class=variant_class,
            vcf_string=vcf_string,
            xref_refseq=xref_refseq,
            norm_func=norm_func
        )


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Vep(brc._BaseRestClient, _Vep):
    """Perform Vep queries
    """
    pass
