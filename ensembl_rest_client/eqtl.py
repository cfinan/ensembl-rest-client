"""Perform queries against the eQTL endpoints
"""
from ensembl_rest_client.base import base_rest_client as brc


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class _Eqtl(object):
    """Perform Eqtl queries
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_eqtl_stable_id(self, species, stable_id, statistic=None,
                           tissue=None, variant_name=None,
                           norm_func=brc.default_pandas):
        """
        Returns the p-value for each SNP in a given gene (e.g.
        ENSG00000227232). GET method. Queries:
        `eqtl/stable_id/:species/:stable_id <https://rest.ensembl.org/documentation/info/species_id>`_.

        Parameters
        ----------
        species : str
            Species name/alias. Example values: 'homo_sapiens'.
        stable_id : str
            Ensembl stable ID. Example values: 'ENSG00000122435'.
        statistic : str or NoneType, optional, default: NoneType
            Filter by statistic. Example values: 'p-value beta'.
        tissue : str or NoneType, optional, default: NoneType
            Tissue of interest [Stomach, Thyroid, Whole_Blood]. Example values:
            'Whole_Blood'.
        variant_name : str or NoneType, optional, default: NoneType
            rsID (Reference SNP cluster ID). Example values: 'rs123'.
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use when obj.pandas is True

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'GET'
        endpoint = 'eqtl/stable_id/{species}/{stable_id}'.format(
            species=species,
            stable_id=stable_id
        )

        return self.rest_query(
            method_type,
            endpoint,
            statistic=statistic,
            tissue=tissue,
            variant_name=variant_name,
            norm_func=norm_func
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_eqtl_variant_name(self, species, variant_name, query_id=None,
                              statistic=None, tissue=None,
                              norm_func=brc.default_pandas):
        """
        Returns the p-values for a SNP (e.g. rs123). GET method. Queries:
        `eqtl/variant_name/:species/:variant_name <https://rest.ensembl.org/documentation/info/species_variant>`_.

        Parameters
        ----------
        species : str
            Species name/alias. Example values: 'homo_sapiens'.
        variant_name : str
            rsID (Reference SNP cluster ID). Example values: 'rs123'.
        query_id : str or NoneType, optional, default: NoneType
            Ensembl stable ID. Example values: 'ENSG00000122435'.
        statistic : str or NoneType, optional, default: NoneType
            Filter by statistic. Example values: 'p-value beta'.
        tissue : str or NoneType, optional, default: NoneType
            Tissue of interest [Stomach, Thyroid, Whole_Blood]. Example values:
            'Whole_Blood'.
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use when obj.pandas is True

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'GET'
        endpoint = 'eqtl/variant_name/{species}/{variant_name}'.format(
            species=species,
            variant_name=variant_name
        )

        return self.rest_query(
            method_type,
            endpoint,
            id=query_id,
            statistic=statistic,
            tissue=tissue,
            norm_func=norm_func
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_eqtl_tissue(self, species, norm_func=brc.default_pandas):
        """
        Returns all tissues currently available in the DB. GET method. Queries:
        `eqtl/tissue/:species/ <https://rest.ensembl.org/documentation/info/tissues>`_.

        Parameters
        ----------
        species : str
            Species name/alias. Example values: 'homo_sapiens'.
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use when obj.pandas is True

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'GET'
        endpoint = 'eqtl/tissue/{species}'.format(
            species=species
        )

        return self.rest_query(
            method_type,
            endpoint,
            norm_func=norm_func
        )


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Eqtl(brc._BaseRestClient, _Eqtl):
    """Perform Eqtl queries
    """
    pass
