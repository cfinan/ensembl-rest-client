"""Perform queries against the overlap endpoints
"""
from ensembl_rest_client.base import base_rest_client as brc


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class _Overlap(object):
    """Perform Overlap queries
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_overlap_id(self, feature, query_id, biotype=None, db_type=None,
                       logic_name=None, misc_set=None, object_type=None,
                       so_term=None, species=None, species_set=None,
                       variant_set=None):
        """Retrieves features (e.g. genes, transcripts, variants and more) that
        overlap a region defined by the given identifier. GET method. Queries:
        `overlap/id/:id <https://rest.ensembl.org/documentation/info/overlap_id>`_.
        Max slice length: 5e6.

        Parameters
        ----------
        feature : `str`
            The type of feature to retrieve. Multiple values are accepted.
            Allowed values: band, gene, transcript, cds, exon, repeat, simple,
            misc, variation, somatic_variation, structural_variation,
            somatic_structural_variation, constrained, regulatory, motif,
            chipseq, array_probe
        query_id : `str`
            An Ensembl stable ID. Example values: 'ENSG00000157764'.
        biotype : `str` or `NoneType`, optional, default: `NoneType`
            The functional classification of the gene or transcript to fetch.
            Cannot be used in conjunction with logic_name when querying
            transcripts. Example values: 'protein_coding'.
        db_type : `str` or `NoneType`, optional, default: `NoneType`
            Restrict the search to a database other than the default. Useful if
            you need to use a DB other than core. Example values: 'core'.
        logic_name : `str` or `NoneType`, optional, default: `NoneType`
            Limit retrieval of genes, transcripts and exons by a given name of
            an analysis.
        misc_set : `str` or `NoneType`, optional, default: `NoneType`
            Miscellaneous set which groups together feature entries. Consult
            the DB or returned data sets to discover what is available. Example
            values: 'cloneset_30k'.
        object_type : `str` or `NoneType`, optional, default: `NoneType`
            Filter by feature type. Example values: 'gene'.
        so_term : `str` or `NoneType`, optional, default: `NoneType`
            Sequence Ontology term to narrow down the possible variants
            returned. Example values: 'SO:0001650'.
        species : `str` or `NoneType`, optional, default: `NoneType`
            Species name/alias. Example values: 'homo_sapiens'.
        species_set : `str` or `NoneType`, optional, default: `NoneType`
            Filter by species set for retrieving constrained elements.
        variant_set : `str` or `NoneType`, optional, default: `NoneType`
            Short name of a set to restrict the variants found. (See list of
            short set names). Example values: 'ClinVar'.
        """
        method_type = 'GET'
        slice_length = 5e6
        endpoint = 'overlap/id/{id}'.format(
            id=query_id
        )

        return self.rest_query(
            method_type,
            endpoint,
            slice_length=slice_length,
            feature=feature,
            biotype=biotype,
            db_type=db_type,
            logic_name=logic_name,
            misc_set=misc_set,
            object_type=object_type,
            so_term=so_term,
            species=species,
            species_set=species_set,
            variant_set=variant_set
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_overlap_region(self, feature, region, species, biotype=None,
                           db_type=None, logic_name=None, misc_set=None,
                           so_term=None, species_set=None,
                           trim_downstream=None, trim_upstream=None,
                           variant_set=None):
        """Retrieves features (e.g. genes, transcripts, variants and more)
        that overlap a given region. GET method. Queries:
        `overlap/region/:species/:region <https://rest.ensembl.org/documentation/info/overlap_region>`_.
        Max slice length: 5e6.

        Parameters
        ----------
        feature : `str`
            The type of feature to retrieve. Multiple values are accepted.
            Allowed values: band, gene, transcript, cds, exon, repeat, simple,
            misc, variation, somatic_variation, structural_variation,
            somatic_structural_variation, constrained, regulatory, motif, peak,
            other_regulatory, array_probe
        region : `str`
            Query region. A maximum of 5Mb is allowed to be requested at any
            one time. Example values: 'X:1..1000:1 X:1..1000:-1 X:1..1000'.
        species : `str`
            Species name/alias. Example values: 'homo_sapiens'.
        biotype : `str` or `NoneType`, optional, default: `NoneType`
            Functional classification of the gene or transcript to fetch.
            Cannot be used in conjunction with logic_name when querying
            transcripts. Example values: 'protein_coding'.
        db_type : `str` or `NoneType`, optional, default: `NoneType`
            Specify the database type to retrieve features from if not using
            the core database. We automatically choose the correct type of DB
            for variation, comparative and regulation features. Example values:
            'core otherfeatures'.
        logic_name : `str` or `NoneType`, optional, default: `NoneType`
            Limit retrieval of genes, transcripts and exons by the name of
            analysis.
        misc_set : `str` or `NoneType`, optional, default: `NoneType`
            Miscellaneous set which groups together feature entries. Consult
            the DB or returned data sets to discover what is available. Example
            values: 'cloneset_30k'.
        so_term : `str` or `NoneType`, optional, default: `NoneType`
            Sequence Ontology term to restrict the variants found. Its
            descendants are also included in the search. Example values:
            'SO:0001650'.
        species_set : `str` or `NoneType`, optional, default: `NoneType`
            The species set name for retrieving constrained elements.
        trim_downstream : `bool` or `NoneType`, optional, default: `NoneType`
            Do not return features which overlap the downstream end of the
            region.
        trim_upstream : `bool` or `NoneType`, optional, default: `NoneType`
            Do not return features which overlap upstream end of the region.
        variant_set : `str` or `NoneType`, optional, default: `NoneType`
            Short name of a set to restrict the variants found. (See list of
            short set names). Example values: 'ClinVar'.
        """
        method_type = 'GET'
        slice_length = 5e6
        endpoint = 'overlap/region/{species}/{region}'.format(
            species=species,
            region=region
        )

        return self.rest_query(
            method_type,
            endpoint,
            slice_length=slice_length,
            feature=feature,
            biotype=biotype,
            db_type=db_type,
            logic_name=logic_name,
            misc_set=misc_set,
            so_term=so_term,
            species_set=species_set,
            trim_downstream=trim_downstream,
            trim_upstream=trim_upstream,
            variant_set=variant_set
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_overlap_translation(self, query_id, db_type=None, feature=None,
                                so_term=None, species=None, data_type=None):
        """Retrieve features related to a specific Translation as described by
        its stable ID (e.g. domains, variants). GET method. Queries:
        `overlap/translation/:id <https://rest.ensembl.org/documentation/info/overlap_translation>`_.
        Max slice length: 5e6.

        Parameters
        ----------
        query_id : `str`
            An Ensembl stable ID. Example values: 'ENSP00000288602'.
        db_type : `str` or `NoneType`, optional, default: `NoneType`
            Restrict the search to a database other than the default. Useful if
            you need to use a DB other than core. Example values: 'core'.
        feature : `str` or `NoneType`, optional, default: `NoneType`
            Specify the type of features requested for the translation. Allowed
            values: transcript_variation, protein_feature, residue_overlap,
            translation_exon, somatic_transcript_variation
        so_term : `str` or `NoneType`, optional, default: `NoneType`
            Sequence Ontology term to restrict the variants found. Its
            descendants are also included in the search. Example values:
            'SO:0001650'.
        species : `str` or `NoneType`, optional, default: `NoneType`
            Species name/alias. Example values: 'homo_sapiens'.
        data_type : `str` or `NoneType`, optional, default: `NoneType`
            Type of data to filter by. By default, all features are returned.
            Can specify a domain or consequence type. Example values:
            'low_complexity'.
        """
        method_type = 'GET'
        slice_length = 5e6
        endpoint = 'overlap/translation/{id}'.format(
            id=query_id
        )

        return self.rest_query(
            method_type,
            endpoint,
            slice_length=slice_length,
            db_type=db_type,
            feature=feature,
            so_term=so_term,
            species=species,
            type=data_type
        )


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Overlap(brc._BaseRestClient, _Overlap):
    """Perform Overlap queries
    """
    pass
