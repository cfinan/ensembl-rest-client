"""
Perform queries against the comparative genomics endpoints
"""
from ensembl_rest_client.base import base_rest_client as brc


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class _ComparativeGenomics(object):
    """Perform ComparativeGenomics queries
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_cafe_genetree_id(self, query_id, compara=None, nh_format=None,
                             norm_func=brc.default_pandas):
        """
        Retrieves a cafe tree of the gene tree using the gene tree stable
        identifier. GET method. Queries:
        `cafe/genetree/id/:id <https://rest.ensembl.org/documentation/info/cafe_tree)>`_`.

        Parameters
        ----------
        query_id : str
            An Ensembl genetree ID. Example values: 'ENSGT00390000003602'.
        compara : str or NoneType, optional, default: NoneType
            Name of the compara database to use. Multiple comparas exist on a
            server for separate species divisions. Example values:
            'vertebrates'.
        nh_format : str or NoneType, optional, default: NoneType
            The format of a NH (New Hampshire) request. Available only with the
            default setting to allow us to return the cafe tree with Taxa names
            appended with number of members and the p_value. example :
            homo_sapiens_3_0.123 where 3 is the number of members and 0.123 is
            the p value.
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'GET'
        endpoint = 'cafe/genetree/id/{id}'.format(
            id=query_id
        )

        return self.rest_query(
            method_type,
            endpoint,
            compara=compara,
            nh_format=nh_format,
            norm_func=norm_func
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_cafe_genetree_member_id(self, query_id, compara=None, db_type=None,
                                    nh_format=None, object_type=None,
                                    species=None,
                                    norm_func=brc.default_pandas):
        """
        Retrieves the cafe tree of the gene tree that contains the gene /
        transcript / translation stable identifier. GET method. Queries:
        `cafe/genetree/member/id/:id <https://rest.ensembl.org/documentation/info/cafe_tree_member_id)>`_.

        Parameters
        ----------
        query_id : str
            An Ensembl stable ID. Example values: 'ENSG00000167664'.
        compara : str or NoneType, optional, default: NoneType
            Name of the compara database to use. Multiple comparas exist on a
            server for separate species divisions. Example values:
            'vertebrates'.
        db_type : str or NoneType, optional, default: NoneType
            Restrict the search to a database other than the default. Useful if
            you need to use a DB other than core. Example values: 'core'.
        nh_format : str or NoneType, optional, default: NoneType
            The format of a NH (New Hampshire) request. Available only with the
            default setting to allow us to return the cafe tree with Taxa names
            appended with number of members and the p_value. example :
            homo_sapiens_3_0.123 where 3 is the number of members and 0.123 is
            the p value.
        object_type : str or NoneType, optional, default: NoneType
            Filter by feature type. Example values: 'gene transcript'.
        species : str or NoneType, optional, default: NoneType
            Species name/alias. Example values: 'homo_sapiens human'.
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'GET'
        endpoint = 'cafe/genetree/member/id/{id}'.format(
            id=query_id
        )

        return self.rest_query(
            method_type,
            endpoint,
            compara=compara,
            db_type=db_type,
            nh_format=nh_format,
            object_type=object_type,
            species=species,
            norm_func=norm_func
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_cafe_genetree_member_symbol(self, species, symbol, compara=None,
                                        db_type=None, external_db=None,
                                        nh_format=None, object_type=None,
                                        norm_func=brc.default_pandas):
        """
        Retrieves the cafe tree of the gene tree that contains the gene
        identified by a symbol. GET method. Queries:
        `cafe/genetree/member/symbol/:species/:symbol <https://rest.ensembl.org/documentation/info/cafe_tree_member_symbol>`_.

        Parameters
        ----------
        species : str
            Species name/alias. Example values: 'homo_sapiens human'.
        symbol : str
            Symbol or display name of a gene. Example values: 'BRCA2'.
        compara : str or NoneType, optional, default: NoneType
            Name of the compara database to use. Multiple comparas exist on a
            server for separate species divisions. Example values:
            'vertebrates'.
        db_type : str or NoneType, optional, default: NoneType
            Restrict the search to a database other than the default. Useful if
            you need to use a DB other than core. Example values: 'core
            otherfeatures'.
        external_db : str or NoneType, optional, default: NoneType
            Filter by external database. Example values: 'HGNC'.
        nh_format : str or NoneType, optional, default: NoneType
            The format of a NH (New Hampshire) request. Available only with the
            default setting to allow us to return the cafe tree with Taxa names
            appended with number of members and the p_value. example :
            homo_sapiens_3_0.123 where 3 is the number of members and 0.123 is
            the p value.
        object_type : str or NoneType, optional, default: NoneType
            Filter by feature type. Example values: 'gene transcript'.
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'GET'
        endpoint = 'cafe/genetree/member/symbol/{species}/{symbol}'.format(
            species=species,
            symbol=symbol
        )

        return self.rest_query(
            method_type,
            endpoint,
            compara=compara,
            db_type=db_type,
            external_db=external_db,
            nh_format=nh_format,
            object_type=object_type,
            norm_func=norm_func
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_family_id(self, query_id, aligned=None, compara=None,
                      member_source=None, sequence=None,
                      norm_func=brc.default_pandas):
        """
        Retrieves a family information using the family stable identifier. GET
        method. Queries:
        `family/id/:id <https://rest.ensembl.org/documentation/info/family>`_.

        Parameters
        ----------
        query_id : str
            An Ensembl family ID. Example values: 'PTHR15573'.
        aligned : bool or NoneType, optional, default: NoneType
            Return the aligned string if true. Otherwise, return the original
            sequence (no insertions). Note: If aligned=1, this will override
            sequence=none.
        compara : str or NoneType, optional, default: NoneType
            Name of the compara database to use. Multiple comparas exist on a
            server for separate species divisions. Example values:
            'vertebrates'.
        member_source : str or NoneType, optional, default: NoneType
            The source of the family members that you want returned. Allowed
            values: all, ensembl, uniprot
        sequence : str or NoneType, optional, default: NoneType
            The type of sequence to bring back. Setting it to none results in
            no sequence being returned. Note: dependant on the setting for
            "aligned", If aligned=1, this will override sequence=none. Allowed
            values: none, cdna, protein
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data

        """
        method_type = 'GET'
        endpoint = 'family/id/{id}'.format(
            id=query_id
        )

        return self.rest_query(
            method_type,
            endpoint,
            aligned=aligned,
            compara=compara,
            member_source=member_source,
            sequence=sequence,
            norm_func=norm_func
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_family_member_id(self, query_id, aligned=None, compara=None,
                             member_source=None, sequence=None,
                             norm_func=brc.default_pandas):
        """
        Retrieves the information for all the families that contains the gene /
        transcript / translation stable identifier. GET method. Queries:
        `family/member/id/:id <https://rest.ensembl.org/documentation/info/family_member_id>`_.

        Parameters
        ----------
        query_id : str
            An Ensembl stable ID. Example values: 'ENSG00000167664'.
        aligned : bool or NoneType, optional, default: NoneType
            Return the aligned string if true. Otherwise, return the original
            sequence (no insertions). Note: If aligned=1, this will override
            sequence=none.
        compara : str or NoneType, optional, default: NoneType
            Name of the compara database to use. Multiple comparas exist on a
            server for separate species divisions. Example values:
            'vertebrates'.
        member_source : str or NoneType, optional, default: NoneType
            The source of the family members that you want returned. Allowed
            values: all, ensembl, uniprot
        sequence : str or NoneType, optional, default: NoneType
            The type of sequence to bring back. Setting it to none results in
            no sequence being returned. Note: dependant on the setting for
            "aligned", If aligned=1, this will override sequence=none. Allowed
            values: none, cdna, protein
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'GET'
        endpoint = 'family/member/id/{id}'.format(
            id=query_id
        )

        return self.rest_query(
            method_type,
            endpoint,
            aligned=aligned,
            compara=compara,
            member_source=member_source,
            sequence=sequence,
            norm_func=norm_func
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_family_member_symbol(self, species, symbol, aligned=None,
                                 compara=None, db_type=None, external_db=None,
                                 member_source=None, object_type=None,
                                 sequence=None, norm_func=brc.default_pandas):
        """
        Retrieves the information for all the families that contains the gene
        identified by a symbol. GET method. Queries:
        `family/member/symbol/:species/:symbol <https://rest.ensembl.org/documentation/info/family_member_symbol>`_.

        Parameters
        ----------
        species : str
            Species name/alias. Example values: 'homo_sapiens human'.
        symbol : str
            Symbol or display name of a gene. Example values: 'BRCA2'.
        aligned : bool or NoneType, optional, default: NoneType
            Return the aligned string if true. Otherwise, return the original
            sequence (no insertions). Note: If aligned=1, this will override
            sequence=none.
        compara : str or NoneType, optional, default: NoneType
            Name of the compara database to use. Multiple comparas exist on a
            server for separate species divisions. Example values:
            'vertebrates'.
        db_type : str or NoneType, optional, default: NoneType
            Restrict the search to a database other than the default. Useful if
            you need to use a DB other than core. Example values: 'core
            otherfeatures'.
        external_db : str or NoneType, optional, default: NoneType
            Filter by external database. Example values: 'HGNC'.
        member_source : str or NoneType, optional, default: NoneType
            The source of the family members that you want returned. Allowed
            values: all, ensembl, uniprot
        object_type : str or NoneType, optional, default: NoneType
            Filter by feature type. Example values: 'gene transcript'.
        sequence : str or NoneType, optional, default: NoneType
            The type of sequence to bring back. Setting it to none results in
            no sequence being returned. Note: dependant on the setting for
            "aligned", If aligned=1, this will override sequence=none. Allowed
            values: none, cdna, protein
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'GET'
        endpoint = 'family/member/symbol/{species}/{symbol}'.format(
            species=species,
            symbol=symbol
        )

        return self.rest_query(
            method_type,
            endpoint,
            aligned=aligned,
            compara=compara,
            db_type=db_type,
            external_db=external_db,
            member_source=member_source,
            object_type=object_type,
            sequence=sequence,
            norm_func=norm_func
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_genetree_id(self, query_id, aligned=None, cigar_line=None,
                        clusterset_id=None, compara=None, nh_format=None,
                        prune_species=None, prune_taxon=None, sequence=None,
                        norm_func=brc.default_pandas):
        """
        Retrieves a gene tree for a gene tree stable identifier. GET method.
        Queries:
        `genetree/id/:id <https://rest.ensembl.org/documentation/info/genetree>`_.

        Parameters
        ----------
        query_id : str
            An Ensembl genetree ID. Example values: 'ENSGT00390000003602'.
        aligned : bool or NoneType, optional, default: NoneType
            Return the aligned string if true. Otherwise, return the original
            sequence (no insertions).
        cigar_line : bool or NoneType, optional, default: NoneType
            Return the aligned sequence encoded in CIGAR format.
        clusterset_id : str or NoneType, optional, default: NoneType
            Name of the gene-tree resource being queried. Common values are
            "default" for the standard multi-clade trees (which exclude all
            non-reference strains) and "murinae" for the trees spanning all
            mouse strains. By default, the most inclusive analysis will be
            selected. Example values: 'default murinae'.
        compara : str or NoneType, optional, default: NoneType
            Name of the compara database to use. Multiple comparas exist on a
            server for separate species divisions. Example values:
            'vertebrates'.
        nh_format : str or NoneType, optional, default: NoneType
            The format of a NH (New Hampshire) request. Allowed values: full,
            display_label_composite, simple, species, species_short_name,
            ncbi_taxon, ncbi_name, njtree, phylip
        prune_species : str or NoneType, optional, default: NoneType
            Prune the tree by species. Supports all species aliases. Will
            return a tree with only the species given. Example values: 'human
            cow'.
        prune_taxon : int or NoneType, optional, default: NoneType
            Prune the tree by taxon. Will return a tree with only the taxons
            given. Example values: '9606 10090'.
        sequence : str or NoneType, optional, default: NoneType
            The type of sequence to bring back. Setting it to none results in
            no sequence being returned. Allowed values: none, cdna, protein
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'GET'
        endpoint = 'genetree/id/{id}'.format(
            id=query_id
        )

        return self.rest_query(
            method_type,
            endpoint,
            aligned=aligned,
            cigar_line=cigar_line,
            clusterset_id=clusterset_id,
            compara=compara,
            nh_format=nh_format,
            prune_species=prune_species,
            prune_taxon=prune_taxon,
            sequence=sequence,
            norm_func=norm_func
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_genetree_member_id(self, query_id, aligned=None, cigar_line=None,
                               clusterset_id=None, compara=None, db_type=None,
                               nh_format=None, object_type=None,
                               prune_species=None, prune_taxon=None,
                               sequence=None, species=None,
                               norm_func=brc.default_pandas):
        """
        Retrieves the gene tree that contains the gene / transcript /
        translation stable identifier. GET method. Queries:
        `genetree/member/id/:id <https://rest.ensembl.org/documentation/info/genetree_member_id>`_.

        Parameters
        ----------
        query_id : str
            An Ensembl stable ID. Example values: 'ENSG00000167664'.
        aligned : bool or NoneType, optional, default: NoneType
            Return the aligned string if true. Otherwise, return the original
            sequence (no insertions).
        cigar_line : bool or NoneType, optional, default: NoneType
            Return the aligned sequence encoded in CIGAR format.
        clusterset_id : str or NoneType, optional, default: NoneType
            Name of the gene-tree resource being queried. Common values are
            "default" for the standard multi-clade trees (which exclude all
            non-reference strains) and "murinae" for the trees spanning all
            mouse strains. By default, the most inclusive analysis will be
            selected. Example values: 'default murinae'.
        compara : str or NoneType, optional, default: NoneType
            Name of the compara database to use. Multiple comparas exist on a
            server for separate species divisions. Example values:
            'vertebrates'.
        db_type : str or NoneType, optional, default: NoneType
            Restrict the search to a database other than the default. Useful if
            you need to use a DB other than core. Example values: 'core'.
        nh_format : str or NoneType, optional, default: NoneType
            The format of a NH (New Hampshire) request. Allowed values: full,
            display_label_composite, simple, species, species_short_name,
            ncbi_taxon, ncbi_name, njtree, phylip
        object_type : str or NoneType, optional, default: NoneType
            Filter by feature type. Example values: 'gene transcript'.
        prune_species : str or NoneType, optional, default: NoneType
            Prune the tree by species. Supports all species aliases. Will
            return a tree with only the species given. Example values: 'human
            cow'.
        prune_taxon : int or NoneType, optional, default: NoneType
            Prune the tree by taxon. Will return a tree with only the taxons
            given. Example values: '9606 10090'.
        sequence : str or NoneType, optional, default: NoneType
            The type of sequence to bring back. Setting it to none results in
            no sequence being returned. Allowed values: none, cdna, protein
        species : str or NoneType, optional, default: NoneType
            Species name/alias. Example values: 'homo_sapiens human'.
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'GET'
        endpoint = 'genetree/member/id/{id}'.format(
            id=query_id
        )

        return self.rest_query(
            method_type,
            endpoint,
            aligned=aligned,
            cigar_line=cigar_line,
            clusterset_id=clusterset_id,
            compara=compara,
            db_type=db_type,
            nh_format=nh_format,
            object_type=object_type,
            prune_species=prune_species,
            prune_taxon=prune_taxon,
            sequence=sequence,
            species=species,
            norm_func=norm_func
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_genetree_member_symbol(self, species, symbol, aligned=None,
                                   cigar_line=None, clusterset_id=None,
                                   compara=None, db_type=None,
                                   external_db=None, nh_format=None,
                                   object_type=None, prune_species=None,
                                   prune_taxon=None, sequence=None,
                                   norm_func=brc.default_pandas):
        """
        Retrieves the gene tree that contains the gene identified by a symbol.
        GET method. Queries:
        `genetree/member/symbol/:species/:symbol <https://rest.ensembl.org/documentation/info/genetree_member_symbol>`_.

        Parameters
        ----------
        species : str
            Species name/alias. Example values: 'homo_sapiens human'.
        symbol : str
            Symbol or display name of a gene. Example values: 'BRCA2'.
        aligned : bool or NoneType, optional, default: NoneType
            Return the aligned string if true. Otherwise, return the original
            sequence (no insertions).
        cigar_line : bool or NoneType, optional, default: NoneType
            Return the aligned sequence encoded in CIGAR format.
        clusterset_id : str or NoneType, optional, default: NoneType
            Name of the gene-tree resource being queried. Common values are
            "default" for the standard multi-clade trees (which exclude all
            non-reference strains) and "murinae" for the trees spanning all
            mouse strains. By default, the most inclusive analysis will be
            selected. Example values: 'default murinae'.
        compara : str or NoneType, optional, default: NoneType
            Name of the compara database to use. Multiple comparas exist on a
            server for separate species divisions. Example values:
            'vertebrates'.
        db_type : str or NoneType, optional, default: NoneType
            Restrict the search to a database other than the default. Useful if
            you need to use a DB other than core. Example values: 'core
            otherfeatures'.
        external_db : str or NoneType, optional, default: NoneType
            Filter by external database. Example values: 'HGNC'.
        nh_format : str or NoneType, optional, default: NoneType
            The format of a NH (New Hampshire) request. Allowed values: full,
            display_label_composite, simple, species, species_short_name,
            ncbi_taxon, ncbi_name, njtree, phylip
        object_type : str or NoneType, optional, default: NoneType
            Filter by feature type. Example values: 'gene transcript'.
        prune_species : str or NoneType, optional, default: NoneType
            Prune the tree by species. Supports all species aliases. Will
            return a tree with only the species given. Example values: 'human
            cow'.
        prune_taxon : int or NoneType, optional, default: NoneType
            Prune the tree by taxon. Will return a tree with only the taxons
            given. Example values: '9606 10090'.
        sequence : str or NoneType, optional, default: NoneType
            The type of sequence to bring back. Setting it to none results in
            no sequence being returned. Allowed values: none, cdna, protein
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'GET'
        endpoint = 'genetree/member/symbol/{species}/{symbol}'.format(
            species=species,
            symbol=symbol
        )

        return self.rest_query(
            method_type,
            endpoint,
            aligned=aligned,
            cigar_line=cigar_line,
            clusterset_id=clusterset_id,
            compara=compara,
            db_type=db_type,
            external_db=external_db,
            nh_format=nh_format,
            object_type=object_type,
            prune_species=prune_species,
            prune_taxon=prune_taxon,
            sequence=sequence,
            norm_func=norm_func
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_alignment_region(self, region, species, aligned=None, compact=None,
                             compara=None, display_species_set=None, mask=None,
                             method=None, species_set=None,
                             species_set_group=None,
                             norm_func=brc.default_pandas):
        """
        Retrieves genomic alignments as separate blocks based on a region and
        species. GET method. Queries:
        `alignment/region/:species/:region <https://rest.ensembl.org/documentation/info/genomic_alignment_region>`_.

        Parameters
        ----------
        region : str
            Query region. A maximum of 10Mb is allowed to be requested at any
            one time. Example values: 'X:1000000..1000100:1
            X:1000000..1000100:-1 X:1000000..1000100'.
        species : str
            Species name/alias. Example values: 'homo_sapiens human'.
        aligned : bool or NoneType, optional, default: NoneType
            Return the aligned string if true. Otherwise, return the original
            sequence (no insertions).
        compact : bool or NoneType, optional, default: NoneType
            Applicable to EPO_EXTENDED alignments. If true, concatenate the
            extended species sequences together to create a single sequence.
            Otherwise, separates out all sequences.
        compara : str or NoneType, optional, default: NoneType
            Name of the compara database to use. Multiple comparas exist on a
            server for separate species divisions. Example values:
            'vertebrates'.
        display_species_set : str or NoneType, optional, default: NoneType
            Subset of species in the alignment to be displayed (multiple
            values). All the species in the alignment will be displayed if this
            is not set. Any valid alias may be used. Example values: 'human
            chimp gorilla'.
        mask : str or NoneType, optional, default: NoneType
            Request the sequence masked for repeat sequences. Hard will mask
            all repeats as N's and soft will mask repeats as lowercased
            characters. Example values: 'hard'. Allowed values: hard,soft
        method : str or NoneType, optional, default: NoneType
            The alignment method. Example values: 'PECAN'. Allowed values: epo,
            epo_extended, pecan, lastz_net, blastz_net, translated_blat_net,
            cactus_hal, cactus_hal_pw
        species_set : str or NoneType, optional, default: NoneType
            The set of species used to define the pairwise alignment (multiple
            values). Should not be used with the species_set_group parameter.
            Use /info/compara/species_sets/:method with one of the methods
            listed above to obtain a valid list of species sets. Any valid
            alias may be used. Example values: 'homo_sapiens mus_musculus'.
        species_set_group : str or NoneType, optional, default: NoneType
            The species set group name of the multiple alignment. Should not be
            used with the species_set parameter. Use
            /info/compara/species_sets/:method with one of the methods listed
            above to obtain a valid list of group names. Example values:
            'mammals, amniotes, fish, sauropsids, murinae'.
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'GET'
        endpoint = 'alignment/region/{species}/{region}'.format(
            species=species,
            region=region
        )

        return self.rest_query(
            method_type,
            endpoint,
            aligned=aligned,
            compact=compact,
            compara=compara,
            display_species_set=display_species_set,
            mask=mask,
            method=method,
            species_set=species_set,
            species_set_group=species_set_group,
            norm_func=norm_func
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_homology_id(self, query_id, aligned=None, cigar_line=None,
                        compara=None, data_format=None, sequence=None,
                        target_species=None, target_taxon=None,
                        data_type=None, norm_func=brc.default_pandas):
        """
        Retrieves homology information (orthologs) by Ensembl gene id. GET
        method. Queries:
        `homology/id/:id <https://rest.ensembl.org/documentation/info/homology_ensemblgene>`_.

        Parameters
        ----------
        query_id : str
            An Ensembl stable ID. Example values: 'ENSG00000157764'.
        aligned : bool or NoneType, optional, default: NoneType
            Return the aligned string if true. Otherwise, return the original
            sequence (no insertions).
        cigar_line : bool or NoneType, optional, default: NoneType
            Return the aligned sequence encoded in CIGAR format.
        compara : str or NoneType, optional, default: NoneType
            Name of the compara database to use. Multiple comparas exist on a
            server for separate species divisions. Example values:
            'vertebrates'.
        data_format : str or NoneType, optional, default: NoneType
            Layout of the response. Allowed values: full, condensed
        sequence : str or NoneType, optional, default: NoneType
            The type of sequence to bring back. Setting it to none results in
            no sequence being returned. Allowed values: none, cdna, protein
        target_species : str or NoneType, optional, default: NoneType
            Filter by species. Supports all species aliases. Example values:
            'human cow'.
        target_taxon : int or NoneType, optional, default: NoneType
            Filter by taxon. Example values: '9606 10090'.
        data_type : str or NoneType, optional, default: NoneType
            The type of homology to return from this call. Projections are
            orthology calls defined between alternative assemblies and the
            genes shared between them. Useful if you need only one type of
            homology back from the service. Allowed values: orthologues,
            paralogues, projections, all
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use when obj.pandas is True

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'GET'
        endpoint = 'homology/id/{id}'.format(
            id=query_id
        )

        return self.rest_query(
            method_type,
            endpoint,
            aligned=aligned,
            cigar_line=cigar_line,
            compara=compara,
            format=data_format,
            sequence=sequence,
            target_species=target_species,
            target_taxon=target_taxon,
            type=data_type,
            norm_func=norm_func
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_homology_symbol(self, species, symbol, aligned=None,
                            cigar_line=None, compara=None, external_db=None,
                            data_format=None, sequence=None,
                            target_species=None, target_taxon=None,
                            data_type=None, norm_func=brc.default_pandas):
        """
        Retrieves homology information (orthologs) by symbol. GET method.
        Queries:
        `homology/symbol/:species/:symbol <https://rest.ensembl.org/documentation/info/homology_symbol>`_.

        Parameters
        ----------
        species : str
            Species name/alias. Example values: 'homo_sapiens human'.
        symbol : str
            Symbol or display name of a gene. Example values: 'BRCA2'.
        aligned : bool or NoneType, optional, default: NoneType
            Return the aligned string if true. Otherwise, return the original
            sequence (no insertions).
        cigar_line : bool or NoneType, optional, default: NoneType
            Return the aligned sequence encoded in CIGAR format.
        compara : str or NoneType, optional, default: NoneType
            Name of the compara database to use. Multiple comparas exist on a
            server for separate species divisions. Example values:
            'vertebrates'.
        external_db : str or NoneType, optional, default: NoneType
            Filter by external database. Example values: 'HGNC'.
        data_format : str or NoneType, optional, default: NoneType
            Layout of the response. Allowed values: full,condensed
        sequence : str or NoneType, optional, default: NoneType
            The type of sequence to bring back. Setting it to none results in
            no sequence being returned. Allowed values: none, cdna, protein
        target_species : str or NoneType, optional, default: NoneType
            Filter by species. Supports all species aliases. Example values:
            'homo_sapiens human'.
        target_taxon : int or NoneType, optional, default: NoneType
            Filter by taxon. Example values: '9606 10090'.
        data_type : str or NoneType, optional, default: NoneType
            The type of homology to return from this call. Projections are
            orthology calls defined between alternative assemblies and the
            genes shared between them. Useful if you need only one type of
            homology back from the service. Allowed values: orthologues,
            paralogues, projections, all
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use when obj.pandas is True

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'GET'
        endpoint = 'homology/symbol/{species}/{symbol}'.format(
            species=species,
            symbol=symbol
        )

        return self.rest_query(
            method_type,
            endpoint,
            aligned=aligned,
            cigar_line=cigar_line,
            compara=compara,
            external_db=external_db,
            format=data_format,
            sequence=sequence,
            target_species=target_species,
            target_taxon=target_taxon,
            type=data_type,
            norm_func=norm_func
        )


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ComparativeGenomics(brc._BaseRestClient, _ComparativeGenomics):
    """Perform ComparativeGenomics queries
    """
    pass
