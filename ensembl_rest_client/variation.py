"""Perform queries against the variation endpoints
"""
from ensembl_rest_client.base import base_rest_client as brc


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class _Variation(object):
    """Perform Variation queries
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_variant_recoder(self, query_id, species, fields=None,
                            norm_func=brc.default_pandas):
        """
        Translate a variant identifier, HGVS notation or genomic SPDI notation
        to all possible variant IDs, HGVS and genomic SPDI. GET method. Queries:
        `variant_recoder/:species/:id <https://rest.ensembl.org/documentation/info/variant_recoder>`_.

        Parameters
        ----------
        query_id : str
            Variant ID, HGVS notation or genomic SPDI notation. Example values:
            'rs56116432 AGT:c.803T>C NC_000023.11:284252:C:G'.
        species : str
            Species name/alias. Example values: 'homo_sapiens human'.
        fields : str or NoneType, optional, default: NoneType
            Comma-separated list of identifiers/notations to include from the
            following types: id (variant ID), hgvsg (HGVS genomic), hgvsc (HGVS
            coding), hgvsp (HGVS protein), spdi (SPDI genomic), vcf_string (VCF
            format).
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use when obj.pandas is True

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'GET'
        endpoint = 'variant_recoder/{species}/{id}'.format(
            species=species,
            id=query_id
        )

        return self.rest_query(
            method_type,
            endpoint,
            fields=fields,
            norm_func=norm_func
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def post_variant_recoder(self, species, ids, fields=None,
                             norm_func=brc.default_pandas):
        """
        Translate a list of variant identifiers, HGVS notations or genomic SPDI
        notations to all possible variant IDs, HGVS and genomic SPDI. POST
        method. Queries:
        `variant_recoder/:species <https://rest.ensembl.org/documentation/info/variant_recoder_post>`_.
        Max post size: 200.

        Parameters
        ----------
        species : str
            Species name/alias. Example values: 'homo_sapiens human'.
        ids : list
            Post option not defined in required/optional args.
        fields : str or NoneType, optional, default: NoneType
            Comma-separated list of identifiers/notations to include from the
            following types: id (variant ID), hgvsg (HGVS genomic), hgvsc (HGVS
            coding), hgvsp (HGVS protein), spdi (SPDI genomic), vcf_string (VCF
            format).
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use when obj.pandas is True

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'POST'
        maximum_post_size = 200
        endpoint = 'variant_recoder/{species}'.format(
            species=species
        )

        return self.rest_query(
            method_type,
            endpoint,
            maximum_post_size=maximum_post_size,
            ids=ids,
            fields=fields,
            norm_func=norm_func
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_variation(self, query_id, species, genotypes=None,
                      genotyping_chips=None, phenotypes=None, pops=None,
                      population_genotypes=None, norm_func=brc.default_pandas):
        """
        Uses a variant identifier (e.g. rsID) to return the variation features
        including optional genotype, phenotype and population data. GET method.
        Queries:
        `variation/:species/:id <https://rest.ensembl.org/documentation/info/variation_id>`_.

        Parameters
        ----------
        query_id : str
            Variant id. Example values: 'rs56116432'.
        species : str
            Species name/alias. Example values: 'homo_sapiens human'.
        genotypes : bool or NoneType, optional, default: NoneType
            Include individual genotypes.
        genotyping_chips : bool or NoneType, optional, default: NoneType
            Include genotyping chips information.
        phenotypes : bool or NoneType, optional, default: NoneType
            Include phenotypes.
        pops : bool or NoneType, optional, default: NoneType
            Include population allele frequencies.
        population_genotypes : bool or NoneType, optional, default: NoneType
            Include population genotype frequencies.
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use when obj.pandas is True

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'GET'
        endpoint = 'variation/{species}/{id}'.format(
            species=species,
            id=query_id
        )

        return self.rest_query(
            method_type,
            endpoint,
            genotypes=genotypes,
            genotyping_chips=genotyping_chips,
            phenotypes=phenotypes,
            pops=pops,
            population_genotypes=population_genotypes,
            norm_func=norm_func
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_variation_pmcid(self, pmcid, species,
                            norm_func=brc.default_pandas):
        """
        Fetch variants by publication using PubMed Central reference number
        (PMCID). GET method. Queries:
        `variation/:species/pmcid/:pmcid <https://rest.ensembl.org/documentation/info/variation_pmcid_get>`_.

        Parameters
        ----------
        pmcid : str
            PubMed Central reference number (PMCID). Example values:
            'PMC5002951'.
        species : str
            Species name/alias. Example values: 'homo_sapiens human'.
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use when obj.pandas is True

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'GET'
        endpoint = 'variation/{species}/pmcid/{pmcid}'.format(
            species=species,
            pmcid=pmcid
        )

        return self.rest_query(
            method_type,
            endpoint,
            norm_func=norm_func
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_variation_pmid(self, pmid, species, norm_func=brc.default_pandas):
        """
        Fetch variants by publication using PubMed reference number (PMID). GET
        method. Queries:
        `variation/:species/pmid/:pmid <https://rest.ensembl.org/documentation/info/variation_pmid_get>`_.

        Parameters
        ----------
        pmid : str
            PubMed reference number (PMID). Example values: '26318936'.
        species : str
            Species name/alias. Example values: 'homo_sapiens human'.
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use when obj.pandas is True

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'GET'
        endpoint = 'variation/{species}/pmid/{pmid}'.format(
            species=species,
            pmid=pmid
        )

        return self.rest_query(
            method_type,
            endpoint,
            norm_func=norm_func
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def post_variation(self, species, ids, genotypes=None, phenotypes=None,
                       pops=None, population_genotypes=None,
                       norm_func=brc.default_pandas):
        """
        Uses a list of variant identifiers (e.g. rsID) to return the variation
        features including optional genotype, phenotype and population data.
        POST method. Queries:
        `variation/:species/ <https://rest.ensembl.org/documentation/info/variation_post>`_.
        Max post size: 200.

        Parameters
        ----------
        species : str
            Species name/alias for the whole batch of symbols. Example values:
            'homo_sapiens human'.
        ids : list
            Post option not defined in required/optional args.
        genotypes : bool or NoneType, optional, default: NoneType
            Include individual genotypes.
        phenotypes : bool or NoneType, optional, default: NoneType
            Include phenotypes.
        pops : bool or NoneType, optional, default: NoneType
            Include population allele frequencies.
        population_genotypes : bool or NoneType, optional, default: NoneType
            Include population genotype frequencies.
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use when obj.pandas is True

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'POST'
        maximum_post_size = 200
        endpoint = 'variation/{species}/'.format(
            species=species
        )

        return self.rest_query(
            method_type,
            endpoint,
            maximum_post_size=maximum_post_size,
            ids=ids,
            genotypes=genotypes,
            phenotypes=phenotypes,
            pops=pops,
            population_genotypes=population_genotypes,
            norm_func=norm_func
        )


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Variation(brc._BaseRestClient, _Variation):
    """Perform Variation queries
    """
    pass
