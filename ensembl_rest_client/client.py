"""Can be used as the main entry point for all the individual clients combined
into a single class (for ease of use)
"""
from ensembl_rest_client.base import base_rest_client as brc
from ensembl_rest_client import (
    archive as a,
    comp_genomics as c,
    eqtl as e,
    info as i,
    ld,
    lookup as lu,
    mapping as m,
    ontology as o,
    overlap as ov,
    phenotypes as ph,
    regulation as reg,
    sequence as seq,
    trans_haplotypes as trh,
    variation as v,
    variation_ga4gh as vg,
    vep,
    xrefs
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Rest(a._Archive, c._ComparativeGenomics, e._Eqtl,
           i._Information, ld._LinkageDisequilibrium, lu._Lookup, m._Mapping,
           o._OntologiesAndTaxonomy, ov._Overlap, ph._PhenotypeAnnotations,
           reg._Regulation, seq._Sequence, trh._TranscriptHaplotypes,
           v._Variation, vg._VariationGa4Gh, vep._Vep, xrefs._CrossReferences,
           brc._BaseRestClient):
    """A single REST client combining all the rest client mix-ins.
    """
    pass
