"""Perform queries against the archive endpoints
"""

from ensembl_rest_client.base import base_rest_client as brc


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class _Archive(object):
    """Perform queries against the archive endpoints
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_archive_id(self, query_id, norm_func=brc.default_pandas):
        """
        Uses the given identifier to return its latest version. GET method.
        Queries:
        `archive/id/:id <https://rest.ensembl.org/documentation/info/archive_id_get>`_

        Parameters
        ----------
        query_id : str
            An Ensembl stable ID. Example values: 'ENSG00000157764'.
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'GET'
        result_format = 'dict'
        endpoint = 'archive/id/{id}'.format(
            id=query_id
        )

        return self.rest_query(
            method_type,
            endpoint,
            norm_func=norm_func
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def post_archive_id(self, query_id, norm_func=brc.default_pandas):
        """
        Retrieve the latest version for a set of identifiers. POST method.
        Queries:
        `archive/id <https://rest.ensembl.org/documentation/info/archive_id_post>`_
        Max post size: 1000.

        Parameters
        ----------
        query_id : list
            Post option not defined in required/optional args.
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data

        """
        method_type = 'POST'
        result_format = 'list'
        maximum_post_size = 1000
        endpoint = 'archive/id'

        return self.rest_query(
            method_type,
            endpoint,
            maximum_post_size=maximum_post_size,
            id=query_id,
            norm_func=norm_func
        )


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Archive(brc._BaseRestClient, _Archive):
    """Perform queries against the archive endpoints
    """
    pass
