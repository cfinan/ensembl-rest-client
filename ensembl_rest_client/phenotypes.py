"""Perform queries against the phenotype annotations endpoints
"""
from ensembl_rest_client.base import base_rest_client as brc


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class _PhenotypeAnnotations(object):
    """Perform PhenotypeAnnotations queries
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_phenotype_accession(self, accession, species,
                                include_children=None, include_pubmed_id=None,
                                include_review_status=None, source=None,
                                norm_func=brc.default_pandas):
        """
        Return phenotype annotations for genomic features given a phenotype
        ontology accession. GET method. Queries:
        `/phenotype/accession/:species/:accession <https://rest.ensembl.org/documentation/info/phenotype_accession>`_.

        Parameters
        ----------
        accession : str
            phenotype ontology accession. Example values: 'EFO:0003900'.
        species : str
            Species name/alias. Example values: 'homo_sapiens human'.
        include_children : bool or NoneType, optional, default: NoneType
            Include annotations attached to child terms.
        include_pubmed_id : bool or NoneType, optional, default: NoneType
            Include the pubmed_ids.
        include_review_status : bool or NoneType, optional, default: NoneType
            Include the review_status information.
        source : str or NoneType, optional, default: NoneType
            Restrict to annotations from a specific source.
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use when obj.pandas is True

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'GET'
        endpoint = '/phenotype/accession/{species}/{accession}'.format(
            species=species,
            accession=accession
        )

        return self.rest_query(
            method_type,
            endpoint,
            include_children=include_children,
            include_pubmed_id=include_pubmed_id,
            include_review_status=include_review_status,
            source=source,
            norm_func=norm_func
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_phenotype_gene(self, gene, species, include_associated=None,
                           include_overlap=None, include_pubmed_id=None,
                           include_review_status=None, include_submitter=None,
                           non_specified=None, trait=None, tumour=None,
                           norm_func=brc.default_pandas):
        """
        Return phenotype annotations for a given gene. GET method. Queries:
        `/phenotype/gene/:species/:gene <https://rest.ensembl.org/documentation/info/phenotype_gene>`_.

        Parameters
        ----------
        gene : str
            Query gene name or Ensembl stable ID. Example values:
            'ENSG00000157764'.
        species : str
            Species name/alias. Example values: 'homo_sapiens human'.
        include_associated : bool or NoneType, optional, default: NoneType
            Include phenotypes associated with variants reporting this gene.
        include_overlap : bool or NoneType, optional, default: NoneType
            Include phenotypes of features overlapping the gene.
        include_pubmed_id : bool or NoneType, optional, default: NoneType
            Include the pubmed_ids.
        include_review_status : bool or NoneType, optional, default: NoneType
            Include the review_status information.
        include_submitter : bool or NoneType, optional, default: NoneType
            Include the submitter names.
        non_specified : bool or NoneType, optional, default: NoneType
            Return non_specified phenotypes (records that did not provide a
            specific phenotype e.g. 'not provided').
        trait : bool or NoneType, optional, default: NoneType
            Return phenotype/disease associations.
        tumour : bool or NoneType, optional, default: NoneType
            Return mutations observed in tumour samples and the tumour type.
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use when obj.pandas is True

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'GET'
        endpoint = '/phenotype/gene/{species}/{gene}'.format(
            species=species,
            gene=gene
        )

        return self.rest_query(
            method_type,
            endpoint,
            include_associated=include_associated,
            include_overlap=include_overlap,
            include_pubmed_id=include_pubmed_id,
            include_review_status=include_review_status,
            include_submitter=include_submitter,
            non_specified=non_specified,
            trait=trait,
            tumour=tumour,
            norm_func=norm_func
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_phenotype_region(self, region, species, feature_type=None,
                             include_pubmed_id=None,
                             include_review_status=None,
                             include_submitter=None, non_specified=None,
                             only_phenotypes=None, trait=None, tumour=None,
                             norm_func=brc.default_pandas):
        """
        Return phenotype annotations that overlap a given genomic region. GET
        method. Queries:
        `/phenotype/region/:species/:region <https://rest.ensembl.org/documentation/info/phenotype_region>`_.

        Parameters
        ----------
        region : str
            Query region. A maximum of 5Mb is allowed to be requested at any
            one time. Example values: '9:22125500-22136000:1
            9:22125500-22136000:-1 9:22125500-22136000'.
        species : str
            Species name/alias. Example values: 'homo_sapiens human'.
        feature_type : str or NoneType, optional, default: NoneType
            Restrict to phenotype annotations from a specific feature type.
            Example values: 'Variation StructuralVariation Gene QTL'.
        include_pubmed_id : bool or NoneType, optional, default: NoneType
            Include the pubmed_ids.
        include_review_status : bool or NoneType, optional, default: NoneType
            Include the review_status information.
        include_submitter : bool or NoneType, optional, default: NoneType
            Include the submitter names.
        non_specified : bool or NoneType, optional, default: NoneType
            Return non_specified phenotypes (records that did not provide a
            specific phenotype e.g. 'not provided').
        only_phenotypes : bool or NoneType, optional, default: NoneType
            Only returns associated phenotype description and mapped ontology
            accessions for a lighter output.
        trait : bool or NoneType, optional, default: NoneType
            Return phenotype/disease associations.
        tumour : bool or NoneType, optional, default: NoneType
            Return mutations observed in tumour samples and the tumour type.
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use when obj.pandas is True

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'GET'
        endpoint = '/phenotype/region/{species}/{region}'.format(
            species=species,
            region=region
        )

        return self.rest_query(
            method_type,
            endpoint,
            feature_type=feature_type,
            include_pubmed_id=include_pubmed_id,
            include_review_status=include_review_status,
            include_submitter=include_submitter,
            non_specified=non_specified,
            only_phenotypes=only_phenotypes,
            trait=trait,
            tumour=tumour,
            norm_func=norm_func
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_phenotype_term(self, species, term, include_children=None,
                           include_pubmed_id=None, include_review_status=None,
                           source=None, norm_func=brc.default_pandas):
        """
        Return phenotype annotations for genomic features given a phenotype
        ontology term. GET method. Queries:
        `/phenotype/term/:species/:term <https://rest.ensembl.org/documentation/info/phenotype_term>`_.

        Parameters
        ----------
        species : str
            Species name/alias. Example values: 'homo_sapiens human'.
        term : str
            phenotype ontology term. Example values: 'coffee consumption'.
        include_children : bool or NoneType, optional, default: NoneType
            Include annotations attached to child terms.
        include_pubmed_id : bool or NoneType, optional, default: NoneType
            Include the pubmed_ids.
        include_review_status : bool or NoneType, optional, default: NoneType
            Include the review_status information.
        source : str or NoneType, optional, default: NoneType
            Restrict to annotations from a specific source.
        norm_func : obj:`func`, optional, default: default_pandas
            The normalisation function to use when obj.pandas is True

        Returns
        -------
        data : list or dict or :obj:`pandas.DataFrame`
            The rest query data
        """
        method_type = 'GET'
        endpoint = '/phenotype/term/{species}/{term}'.format(
            species=species,
            term=term
        )

        return self.rest_query(
            method_type,
            endpoint,
            include_children=include_children,
            include_pubmed_id=include_pubmed_id,
            include_review_status=include_review_status,
            source=source,
            norm_func=norm_func
        )


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class PhenotypeAnnotations(brc._BaseRestClient, _PhenotypeAnnotations):
    """Perform PhenotypeAnnotations queries
    """
    pass
