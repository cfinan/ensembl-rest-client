# Getting Started with enembl-rest-client
__version__: `0.1.3a0`

enembl-rest-client provides Pythonic access to the Ensembl REST services. It implements most the current endpoints at `http://rest.ensembl.org`. You can also use `https://grch37.rest.ensembl.org` of access to the Human GRCh37 assembly, however, some of the endpoint methods will not work (I think).

Note that not all the implemented endpoint methods have been tested properly. They are being tested as they are used and having a pytest test written for them.

There is [online](https://cfinan.gitlab.io/ensembl-rest-client/index.html) documentation for ensembl-rest-client.

## Installation instructions
At present, ensembl-rest-client is undergoing development and no packages exist yet on PyPi. Therefore it is recommended that it is installed in either of the two ways listed below.

### Installation using pip
You can install using pip from the root of the cloned repository, first clone and cd into the repository root:

```
git clone git@gitlab.com:cfinan/ensembl-rest-client.git
cd ensembl-rest-client
```

Install the dependencies:
```
python -m pip install --upgrade -r requirements.txt
```

Then install using pip
```
python -m pip install .
```

Or for an editable (developer) install run the command below from the root of the repository. The difference with this is that you can just to a `git pull` to update the package, or switch branches without re-installing:
```
python -m pip install -e .
```

### Installation using conda
I maintain a conda package in my personal conda channel. To install this please run:

```
conda install -c cfin -c conda-forge ensembl-rest-client
```

### Conda dependencies
There are also conda yaml environment files in `./resources/conda/env` that have the same contents as `requirements.txt` but for conda packages, so all the pre-requisites. I use this to install all the requirements via conda and then install the package as an editable pip install.

However, if you find these useful then please use them. There are Conda environments for Python v3.8, v3.9 and v3.10.

## Basic usage
There are some examples in `./resources/examples` where `.` is the root of the merge-sort repository.

## Run tests
If you have cloned the repository, you can also run the tests using `pytest ./tests`, if any fail please contact us (see the contribution page for contact info).
