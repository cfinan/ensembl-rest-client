"""Tests for the Ensembl REST client overlap endpoints
"""
from ensembl_rest_client import overlap
# from requests.exceptions import HTTPError
# import pytest
# import pprint as pp


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def test_get_overlap_region(ping):
    """Test a single base pair overlap detects a known variant
    """
    chr_name = '1'
    start_pos = 230710048
    end_pos = 230710048
    query = '{0}:{1}..{2}'.format(
        chr_name, start_pos, end_pos
    )
    rc = overlap.Overlap(ping=False)
    data = rc.get_overlap_region(
        'variation', query, 'homo_sapiens'
    )
    assert any([i['id'] == 'rs699' for i in data]), "variant missing"

