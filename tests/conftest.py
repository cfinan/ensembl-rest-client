"""
Fixtures for the merit test
"""
import requests
import pytest
import os
import shutil
import pprint as pp


FIXTURE_DIR = os.path.join(
    os.path.dirname(os.path.realpath(__file__)),
    'data_files',
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.fixture(scope="session", autouse=True)
def ping():
    """
    Ensure the Ensembl REST server is up before starting the test
    """
    ping_endpoint = 'https://rest.ensembl.org/info/ping?'
    r = requests.get(
        ping_endpoint,
        headers={"Content-Type": "application/json"}
    )

    if not r.ok:
        r.raise_for_status()

    decoded = r.json()
    return bool(int(decoded['ping']))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.fixture()
def archive_genes():
    """
    Ensure the Ensembl REST server is up before starting the test
    """
    gene_file = os.path.join(FIXTURE_DIR, 'archive_genes.txt')
    with open(gene_file) as infile:
        genes = [g.strip() for g in infile]
    return genes


# # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# def setup_file(infile, outdir):
#     """
#     """
#     outfile = os.path.join(outdir, os.path.basename(infile))
#     shutils.copy2(infile, outdir)
#     return outfile
