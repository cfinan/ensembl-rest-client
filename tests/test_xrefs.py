"""Tests for the xrefs module
"""
from ensembl_rest_client import xrefs
# import pprint as pp


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def test_get_xrefs_symbol(ping):
    """Make sure that conversion from HGNC symbols to ensembl genes is working
    ok
    """
    species = 'homo_sapiens'
    symbol = "BRCA2"
    external_db = "HGNC"

    rc = xrefs.CrossReferences(ping=False)
    data = rc.get_xrefs_symbol(species, symbol, external_db=external_db)
    
    # Is the outer data structure the correct type
    assert isinstance(data, list), "wrong data type"

    # Now make sure the elements of the list are dictionaries
    assert all([isinstance(i, dict) for i in data]), \
        "data elements are the wrong type"

    # Do we have the expected number of records
    assert len(data) == 2, "incorrect number of records"
