"""
Tests for Ensembl REST lookup endpoints
"""
from ensembl_rest_client import lookup
from requests.exceptions import HTTPError
import pytest
import pprint as pp


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def test_get_lookup_id(ping):
    """
    Test the get query for IDs
    """
    query_gene = "ENSG00000157764"
    rc = lookup.Lookup(ping=False)
    data = rc.get_lookup_id(query_gene)
    assert isinstance(data, dict), "results are wrong datatype"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def test_get_lookup_id_fail(ping):
    """
    This get query should fail as the ID is not a latest ID
    """
    query_gene = "ENSG00000124529"
    rc = lookup.Lookup(ping=False)

    with pytest.raises(HTTPError) as e:
        rc.get_lookup_id(query_gene)
        assert e.response.status_code == 400


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def test_post_lookup_id(ping):
    """
    Test post query or IDs
    """
    query_genes = ["ENSG00000157764", "ENSG00000124529", "ENSG00000248378"]
    rc = lookup.Lookup(ping=False)
    data = rc.post_lookup_id(query_genes)

    assert isinstance(data, dict), "results are wrong datatype"
    assert len(data) == 3, "unexpected data length"
    assert data['ENSG00000124529'] is None, \
        "expected ENSG00000124529 to be NoneType"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def test_post_lookup_id_pandas(ping):
    """
    Test post query or IDs
    """
    query_genes = ["ENSG00000157764", "ENSG00000124529", "ENSG00000248378"]
    rc = lookup.Lookup(ping=False, pandas=False)
    data = rc.post_lookup_id(query_genes, data_format='full', expand=True)
    pp.pprint(data)
    # assert isinstance(data, dict), "results are wrong datatype"
    # assert len(data) == 3, "unexpected data length"
    # assert data['ENSG00000124529'] is None, \
    #     "expected ENSG00000124529 to be NoneType"

    # pp.pprint(data)
