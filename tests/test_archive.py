from ensembl_rest_client import archive


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def test_archive_get(ping, archive_genes):
    """
    Test that the archive GET query works and gives the expected results
    """
    result = {'ENSG00000107618': ['ENSG00000265203']}
    rc = archive.Archive()
    data = rc.get_archive_id('ENSG00000107618')
    assert_archive_results([data], result)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def test_archive_post(ping, archive_genes):
    """
    Test that the archive POST query works and gives the expected results
    """
    results = {
        'ENSG00000124529': ['ENSG00000273542', 'ENSG00000274618',
                            'ENSG00000275126', 'ENSG00000276966',
                            'ENSG00000277157', 'ENSG00000278637',
                            'ENSG00000278705'],
        'ENSG00000124578': ['ENSG00000273542', 'ENSG00000274618',
                            'ENSG00000275126', 'ENSG00000275663',
                            'ENSG00000278637'],
        'ENSG00000124693': ['ENSG00000274267', 'ENSG00000274750',
                            'ENSG00000275379', 'ENSG00000275714',
                            'ENSG00000277775', 'ENSG00000278272']
    }

    rc = archive.Archive()
    data = rc.post_archive_id(list(results.keys()))
    assert_archive_results(data, results)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def assert_archive_results(data, results):
    """
    Evaluate the results from an archive get/post query

    Parameters
    ----------
    data : list of dict
        The data returned from the REST query
    results : dict of list
        The keys are the search genes the values are replacement genes
    """
    for i in data:
        assert i['id'] in results, "unexpected id in returned data"
        for j in i['possible_replacement']:
            assert j['stable_id'] in results[i['id']]
