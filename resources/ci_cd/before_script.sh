#!/bin/sh
# This is assumed to be running from the root of the repo
echo "[before] running in: $PWD"
# For debugging
echo $CI_COMMIT_BRANCH
echo $CI_DEFAULT_BRANCH
python --version
git branch

# Needed for Sphinx (make in build-base)
apk add build-base

# Upgrade pip first
pip install --upgrade pip

# Sphinx doc building requirements
pip install -r resources/docker/sphinx.txt

# Package requirements
pip install -r requirements.txt

# Now upgrade Numpy as I am
# Unable to import required dependencies:
# numpy:

# IMPORTANT: PLEASE READ THIS FOR ADVICE ON HOW TO SOLVE THIS ISSUE!

# Importing the numpy C-extensions failed. This error can happen for
# many reasons, often due to issues with your setup or how NumPy was
# installed.

# We have compiled some common reasons and troubleshooting tips at:

# https://numpy.org/devdocs/user/troubleshooting-importerror.html

# Please note and check the following:

# * The Python version is: Python3.9 from "/usr/local/bin/python"
# * The NumPy version is: "1.23.2"

# and make sure that they are the versions you expect.
# Please carefully study the documentation linked above for further help.

# Original error was: Error loading shared library libopenblas.so.3: No such file or directory (needed by /usr/local/lib/python3.9/site-packages/numpy/core/_multiarray_umath.cpython-39-x86_64-linux-gnu.so)
pip install numpy --upgrade

# Install the package being built/tested
pip install .

# Run pytest
