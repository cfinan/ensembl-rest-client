ensembl\_rest\_client package
=============================

Submodules
----------

ensembl\_rest\_client.archive module
------------------------------------

.. automodule:: ensembl_rest_client.archive
   :members:
   :undoc-members:
   :show-inheritance:

ensembl\_rest\_client.comp\_genomics module
-------------------------------------------

.. automodule:: ensembl_rest_client.comp_genomics
   :members:
   :undoc-members:
   :show-inheritance:

ensembl\_rest\_client.eqtl module
---------------------------------

.. automodule:: ensembl_rest_client.eqtl
   :members:
   :undoc-members:
   :show-inheritance:

ensembl\_rest\_client.info module
---------------------------------

.. automodule:: ensembl_rest_client.info
   :members:
   :undoc-members:
   :show-inheritance:

ensembl\_rest\_client.ld module
-------------------------------

.. automodule:: ensembl_rest_client.ld
   :members:
   :undoc-members:
   :show-inheritance:

ensembl\_rest\_client.lookup module
-----------------------------------

.. automodule:: ensembl_rest_client.lookup
   :members:
   :undoc-members:
   :show-inheritance:

ensembl\_rest\_client.mapping module
------------------------------------

.. automodule:: ensembl_rest_client.mapping
   :members:
   :undoc-members:
   :show-inheritance:

ensembl\_rest\_client.ontology module
-------------------------------------

.. automodule:: ensembl_rest_client.ontology
   :members:
   :undoc-members:
   :show-inheritance:

ensembl\_rest\_client.overlap module
------------------------------------

.. automodule:: ensembl_rest_client.overlap
   :members:
   :undoc-members:
   :show-inheritance:

ensembl\_rest\_client.phenotypes module
---------------------------------------

.. automodule:: ensembl_rest_client.phenotypes
   :members:
   :undoc-members:
   :show-inheritance:

ensembl\_rest\_client.regulation module
---------------------------------------

.. automodule:: ensembl_rest_client.regulation
   :members:
   :undoc-members:
   :show-inheritance:

ensembl\_rest\_client.sequence module
-------------------------------------

.. automodule:: ensembl_rest_client.sequence
   :members:
   :undoc-members:
   :show-inheritance:

ensembl\_rest\_client.trans\_haplotypes module
----------------------------------------------

.. automodule:: ensembl_rest_client.trans_haplotypes
   :members:
   :undoc-members:
   :show-inheritance:

ensembl\_rest\_client.variation module
--------------------------------------

.. automodule:: ensembl_rest_client.variation
   :members:
   :undoc-members:
   :show-inheritance:

ensembl\_rest\_client.variation\_ga4gh module
---------------------------------------------

.. automodule:: ensembl_rest_client.variation_ga4gh
   :members:
   :undoc-members:
   :show-inheritance:

ensembl\_rest\_client.vep module
--------------------------------

.. automodule:: ensembl_rest_client.vep
   :members:
   :undoc-members:
   :show-inheritance:

ensembl\_rest\_client.xrefs module
----------------------------------

.. automodule:: ensembl_rest_client.xrefs
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: ensembl_rest_client
   :members:
   :undoc-members:
   :show-inheritance:
