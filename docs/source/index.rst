.. ensembl-rest-client documentation master file, created by
   sphinx-quickstart on Sat Oct 17 17:24:48 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ensembl-rest-client
==============================

`ensembl-rest-client <https://gitlab.com/cfinan/ensembl-rest-client>`_ provides a Python programming interface to the `Ensembl REST API <https://rest.ensembl.org/>`_. This respects the server wait times and also provides an experimental option to cache query results that can speed up the debugging process and prevent excess queries against the server.

.. toctree::
   :maxdepth: 2
   :caption: Setup:

   getting_started

.. toctree::
   :maxdepth: 2
   :caption: Programmer reference

   examples/examples
   api/modules

.. toctree::
   :maxdepth: 2
   :caption: Project admin

   contributing


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
